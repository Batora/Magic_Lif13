PROJET LIF13 - DEVELOPPEMENT JAVA  <br>

APPLICATION : Jeu de cartes de type "Combat de cr�atures"

/* GROUPE  */ <br>


	-OTHOMENE Thibaut

	-PORTET Marc



****************************************** ~ RESUME DU PROJET : ~ *************************************** </br>


Depuis les ann�es 1990, plusieurs jeux proposent � des joueurs d'incarner des 
mages s'affrontant en invoquant des cr�atures imaginaires, repr�sent�es par des
cartes � jouer. De plus, divers jeux-vid�os s'inspirent de ceux-ci et
proposent de jouer en ligne � ce type de jeux. Chacun de ces jeux propose ses 
propres r�gles et particularit�s.<br> 
Objectif du projet : R�aliser un jeu pour 2 joueurs en s'appuyant sur les r�gles
suivantes : <br>

REGLES :<br>

	- Chaque joueur commence avec un total de 10 points de vie et seulement 
	4 cartes en main (il n'y a pas de pioche dans ce jeu).Chaque joueur joue �
	tour de r�le, le joueur dont c'est le tour est appel� "Joueur Actif", l'autre 
	est appel� "Joueur Adverse". Lorsqu'un joueur atteint 0pv, il perd imm�diatement
	la partie et son adversaire est d�clar� vainqueur.

	- Pour s'affronter, les joueurs vont devoir invoquer en jeu des cartes pr�sentes
	dans leurs mains. Dans un m�me tour de jeu, le nombre et la qualit� des cartes 
	invoqu�es seront limit�s par le nombre de ressources dont dispose le joueur 
	actif. Dans un premier temps (i.e, sans extension), toutes les cartes � la 
	disposition des joueurs sont des cartes de cr�atures. 

	- Une carte de cr�ature permet d'attaquer le joueur adverse pour tenter de lui faire perdre
	des points de vie, ou de se d�fendre en bloquant les attaques adverses. Elle dispose
	d'un nom, d'un co�t en ressources, et de caract�ristiques. 



****************************************** ~ S'ORGANISER SUR LE PROJET ~ ****************************************** </br>

Pour d�buter :

	- git clone adressegitdudepot  		 //cr�e le r�pertoire Magic_Lif13 dans le r�pertoire courant et clone le git dans ce r�pertoire

	- git checkout -b nomdelabranche 	 //cr�er une branche et basculer sur celle-ci

	- git checkout nomdelabranche		 //basculer sur la branche


Il faut faire son travail dans ce r�pertoire, une fois termin�, faire une demande de merge de la mani�re suivante :


Sur GitLab :

	- aller dans le projet Magic_LIF13

	- cliquer sur Merge Requests

	- New Merge Request

	- Indiquer les sources (sa branche) et la cible (master)

	- Cliquer sur Compare branches

	- R�sumer de mani�re concise les modifications qu'apportent la branche au master et choisir qui doit valider


Et dans le doute (--reboot--) :
	http://rogerdudler.github.io/git-guide/index.fr.html