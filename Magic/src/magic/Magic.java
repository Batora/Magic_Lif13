
package magic;

import javax.swing.JFrame;
import magic.modele.*;
import magic.vue.*;
import magic.controleur.*;

/**
 *
 * @author P1302812 & P1509371
 */
public class Magic {

    public static Jeu jeu;
    
     public static void initialiserPartie(){
             
        // On initialise les cartes
        Creature c1=new Creature(0,0,3,1,1,"c1");
        Creature c2=new Creature(0,1,3,1,2,"c2");
        Creature c3=new Creature(0,2,3,1,3,"c3");
        Creature c4=new Creature(0,3,3,1,4,"c4");        
        Creature c5=new Creature(5,0,3,1,1,"c5");
        Creature c6=new Creature(5,1,3,1,2,"c6");
        Creature c7=new Creature(5,2,3,1,3,"c7");
        Creature c8=new Creature(5,3,3,1,4,"c8");
        
        
        
        // On initialise la vue des cartes creatures
        
        VueCarteCreature vC1 = new VueCarteCreature(c1);
        VueCarteCreature vC2 = new VueCarteCreature(c2);
        VueCarteCreature vC3 = new VueCarteCreature(c3);
        VueCarteCreature vC4 = new VueCarteCreature(c4);
        VueCarteCreature vC5 = new VueCarteCreature(c5);
        VueCarteCreature vC6 = new VueCarteCreature(c6);
        VueCarteCreature vC7 = new VueCarteCreature(c7);
        VueCarteCreature vC8 = new VueCarteCreature(c8);
        
        
        // On fait 2 tableau de vue de carte de creature
        
        VueCarteCreature[] deckVu1 = { vC1, vC2, vC3, vC4};
        VueCarteCreature[] deckVu2 = { vC5, vC6, vC7, vC8};
        

        // On initialise les decks des joueurs
        Carte mainJ1[]={c1,c2,c3, c4};
        Carte mainJ2[]={c5,c6,c7,c8};
        
        // On initialise les joueurs
        JoueurHumain j1 = new JoueurHumain(1, "Alice", mainJ1);
        JoueurHumain j2 = new JoueurHumain(2, "Bob", mainJ2);
        
        Plateau plt=new Plateau();
        
        
        Joueur listeJ[] = {j1,j2};
  
        
        // On initialise une partie
        MainJoueur mj1 = new MainJoueur(j1,plt);
        MainJoueur mj2 = new MainJoueur(j2,plt);
        mj1.setCartes(mainJ1);
        mj2.setCartes(mainJ2);
        
        j1.setJmain(mj1);
        j2.setJmain(mj2);
        
        Jeu newGame = new Jeu(listeJ,plt);
        newGame.getPlateau().initialiser(listeJ);
              // On place les cartes dans les mains des joueurs
        newGame.initialiserMain(listeJ);
        
        
        // On détermine les mains des joueurs
        MainJoueur MJ1 = newGame.getPlateau().getMainJoueur(j1);
        MainJoueur MJ2 = newGame.getPlateau().getMainJoueur(j2);
        
        
        Carte[] cartes1 = new Carte[4];
        Carte[] cartes2 = new Carte[4];
        
        
        cartes1 = MJ1.getCartes();      
        
             
        
        // On affiche le plateau -- MODE CONSOLE --
        newGame.getPlateau().afficherPlateau();
        
        Phase pha=Phase.defense;
     
        
        newGame.setPhaseActive(pha);
        newGame.setNumPhase(0);
        
        
       
        Magic.jeu=newGame;
        
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
         // On initialise les cartes
    
        
        Magic.initialiserPartie();
        Controleur ctrl = new ControleurVueNewGame(jeu);
        JFrame menu = new MenuPrincipal(ctrl);

        System.out.println("carte test jeu : " + Magic.jeu.getJ1().getJmain().getCarte(0));
        System.out.println("carte test plateau : " + Magic.jeu.getPlateau().getMainJoueur(Magic.jeu.getJ1()).getCarte(1));
      
    }

      
}
