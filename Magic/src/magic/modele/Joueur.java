package magic.modele;
import java.util.Observable;
import javax.swing.JOptionPane;

/**
 * Classe Abstraite de Joueur, généralisations des méthodes communes aux IA/Humains
 * @author P1302812 & P1509371
 */
public abstract class Joueur extends Observable {

    /**
     * PDV = Points de Vie du joueur
     */
    protected int PDV;
    private int PDVMax;
    protected String pseudo;
    protected int nbCarte;
    /**
     * Le nombre de ressources disponibles pour le joueur pour faire des invocations
     */
    protected int ressource;
    protected int ressourceMax;
    protected int id;
    /**
     * Les cartes possédées par un joueur
     */
    protected Carte deck[];
    
    private MainJoueur jmain;
    
    private TerrainJoueur jterrain;
    
    private LigneCombat jligneCombat;
    
    /**
     * Constructeur de Joueur
     * @param id Numéro identifiant le joueur
     * @param pseudo nom pour l'affichage
     * @param cartes Le deck du joueur
     */
    public Joueur(int id, String pseudo, Carte[] cartes){
        this.PDVMax=10;
        this.PDV=PDVMax;
        this.nbCarte=4;
        this.ressource=0;
        this.ressourceMax=0;
        this.deck= cartes;
    }
    
    /**
     * Récupérer l'ID d'un joueur
     * @return Id du joueur
     */
    public int getId(){
        return this.id;
    }

    /**
     * On va placer la carte sur le plateau de jeu
     * @param plt Plateau de jeu
     * @param c Carte que le joueur veut poser
     */
    public abstract void poserCarte(Plateau plt, Carte c);

    /**
     * @return the PDV
     */
    public int getPDV() {
        return PDV;
    }

    /**
     * @param PDV the PDV to set
     */
    public void setPDV(int PDV) {
        this.PDV = PDV;
        if(this.PDV<=0){
            System.out.println("Vous avez perdu !");
            JOptionPane.showMessageDialog(null, "Vous avez perdu ! ");
            
            System.exit(0);
        }
    }

    /**
     * @return the pseudo
     */
    public String getPseudo() {
        return pseudo;
    }

    /**
     * @param pseudo the pseudo to set
     */
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    /**
     * @return the nbCarte
     */
    public int getNbCarte() {
        return nbCarte;
    }

    /**
     * @param nbCarte the nbCarte to set
     */
    public void setNbCarte(int nbCarte) {
        this.nbCarte = nbCarte;
    }

    /**
     * @return the ressource
     */
    public int getRessource() {
        return ressource;
    }

    /**
     * @param ressource the ressource to set
     */
    public void setRessource(int ressource) {
        this.ressource = ressource;
        setChanged();
        notifyObservers(this.ressource);
    }
    
    

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the deck
     */
    public Carte[] getDeck() {
        return deck;
    }

    /**
     * @param deck the deck to set
     */
    public void setDeck(Carte[] deck) {
        this.deck = deck;
    }

    /**
     * @return the jterrain
     */
    public TerrainJoueur getJterrain() {
        return jterrain;
    }

    /**
     * @param jterrain the jterrain to set
     */
    public void setJterrain(TerrainJoueur jterrain) {
        this.jterrain = jterrain;
    }

    /**
     * @return the jligneCombat
     */
    public LigneCombat getJligneCombat() {
        return jligneCombat;
    }

    /**
     * @param jligneCombat the jligneCombat to set
     */
    public void setJligneCombat(LigneCombat jligneCombat) {
        this.jligneCombat = jligneCombat;
    }

    /**
     * @return the ressourceMax
     */
    public int getRessourceMax() {
        return ressourceMax;
    }

    /**
     * @param ressourceMax the ressourceMax to set
     */
    public void setRessourceMax(int ressourceMax) {
        this.ressourceMax = ressourceMax;
        setChanged();
        notifyObservers(this.ressourceMax);
    }

    /**
     * @return the jmain
     */
    public MainJoueur getJmain() {
        return jmain;
    }

    /**
     * @param jmain the jmain to set
     */
    public void setJmain(MainJoueur jmain) {
        this.jmain = jmain;
    }

    /**
     * @return the PDVMax
     */
    public int getPDVMax() {
        return PDVMax;
    }

    /**
     * @param PDVMax the PDVMax to set
     */
    public void setPDVMax(int PDVMax) {
        this.PDVMax = PDVMax;
    }
    
    

}