
package magic.modele;

/**
 * Une classe abstraite qui va permettre de généraliser le comportement d'une IA
 * @author P1302812 & P1509371
 */
public abstract class JoueurIA extends Joueur {

    /**
     * Constructeur de JoueurIA
     */
    public JoueurIA(int id, String pseudo, Carte[] cartes){
        super(id, pseudo, cartes);
        this.pseudo=pseudo;
        this.id=id;         
    }
}
