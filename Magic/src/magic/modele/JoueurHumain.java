
package magic.modele;

/**
 * Comportement d'un Joueur Humain
 * @author P1302812 & P1509371
 */
public class JoueurHumain extends Joueur {

    /**
     * Constructeur de Joueur Humain
     * @param pseudo
     * @param id
     */
    public JoueurHumain(int id, String pseudo, Carte[] cartes){
        super(id, pseudo, cartes);
        this.pseudo=pseudo;
        this.id=id;
    }


    @Override
    public void poserCarte(Plateau plt,Carte c){
        if(c.getPosX()==0){
            if(plt.getMains()[this.id].getCartes()[c.getPosY()]==null){
                plt.getMains()[this.id].ajouterCarte(this.id, c);
            }            
        }
        if(c.getPosX()==1){
            if(plt.getTerrains()[this.id].getCartes()[c.getPosY()]==null){
                plt.getTerrains()[this.id].ajouterCarte(this.id, c);
            }       
        }        
        if(c.getPosX()==2){
            if(plt.getLignesCombat()[this.id].getCartes()[c.getPosY()]==null){
                plt.getLignesCombat()[this.id].ajouterCarte(this.id, c);
            }       
        }         
    }
 
}
