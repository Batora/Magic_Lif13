
package magic.modele;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Modèle de l'application contenant le "code métier"
 * @author P1302812 & P1509371
 */
public class Jeu extends Observable{

    private int nbTour;
    protected int nbCartePose;
    private Joueur joueurActif;
    private Joueur joueurAdverse;
    private Joueur listeJoueur[];
    private boolean partieFinie;
    private Phase phaseActive;
    private int numPhase;
    protected Plateau plateau;
    
    private ArrayList<Observer> listObserver = new ArrayList<>();
    
    /**
     * Constructeur Jeu
     * @param listeJoueur les joueurs de la partie
     * @param plt le plateau de jeu
     */
    public Jeu(Joueur listeJoueur[],Plateau plt){
        this.nbTour=1;
        this.nbCartePose=0;
        this.listeJoueur=listeJoueur;
        this.joueurActif=listeJoueur[0];
        this.joueurAdverse=listeJoueur[1];
                
        this.plateau=plt;
        
        this.joueurActif.setRessource(1);
        this.joueurActif.setRessourceMax(1);
        this.joueurAdverse.setRessource(1);
        this.joueurAdverse.setRessourceMax(1);
        
        partieFinie=false;       

        
    }  
        
    public Plateau getPlateau(){
        return this.plateau;
    }
    
    /**
     * Méthode qui va placer les mains des joueurs sur le plateau
     * @param listeJoueur liste des Joueurs
     */
    public void initialiserMain(Joueur listeJoueur[]){
        this.plateau.initialiser(listeJoueur);
        notifyObservers();
    }
    
    /**
     * Phase de combat entre 2 cartes face à face
     * @param plt Plateau de jeu
     * @param c1 Creature du Joueur Actif
     * @param c2 Creature du Joueur Adverse
     */
    public void combatCreatures(Plateau plt,Creature c1, Creature c2){
       
        //On regarde quelle créature meurt au combat
        boolean tuerc2=false;
        boolean tuerc1=false;
        if(c1.getAtk()>=c2.getDef()){
            System.out.println("tuer carte1");
            tuerc2=true;
        }
        if(c1.getDef()<=c2.getAtk()){
            System.out.println("tuer carte2");
            tuerc1=true;
        }

        
        //Si elles ne sont pas mortes, on les replace à leur position d'origine et on les fatigue
        if(!tuerc1){
            plt.getTerrainJoueur(this.getJoueurActif()).ajouterCarte(c1.getPosY(),c1);
            plt.getLigneCombatJoueur(this.getJoueurActif()).getCartes()[c1.getPosY()]=null;   
            c1.setEstFatigue(true);
        }
        if(!tuerc2){
            plt.getTerrainJoueur(this.getJoueurAdverse()).ajouterCarte(c2.getPosY(),c2);
            plt.getTerrainJoueur(this.getJoueurAdverse()).getCartes()[c2.getPosY()]=c2;
            c2.setEstFatigue(true);

        }

    }
    
    /**
     * Gere les dégats des monstres infligés directement au joueur actif
     * @param plt Plateau de Jeu
     * @param c1 Créature de l'adversaire, qui attaque directement le joueur actif
     */
    public void attaqueDirecte(Plateau plt, Creature c1){
         this.getJoueurActif().setPDV(getJoueurActif().getPDV()-c1.getAtk());
         
         plt.getTerrainJoueur(this.getJoueurAdverse()).ajouterCarte(c1.getPosY(), c1);
         c1.setEstFatigue(true);
         plt.getLigneCombatJoueur(this.getJoueurAdverse()).supprimerCarte(c1.getPosY());  
  
         //On supprime les cartes dans tous les cas
    }
    
    /**
     * Méthode qui permet de changer de tour, on fera tous les appels aux fonctions 
     * qui vont venir gérer le jeu là dedans
     */
    public void nextPhase(){
        Phase pha = Phase.values()[this.getNumPhase()-1];
        System.out.println("pha :" + pha);
        
            if(pha != null && pha==this.getPhaseActive())switch (pha) {
                case defense:   
                    boolean hasCartes=false;
                    //un booleen pour savoir si l'adversaire a des cartes dans la ligne de combat
                    //On vérifie d'abord que le joueur d'en face à des cartes sur sa ligne de combat (sinon pas de déplacement--> pas de défense)
                    for(int i=0; i<this.getPlateau().getLigneCombatJoueur(this.getJoueurAdverse()).getCartes().length;i++){
                         if(this.getPlateau().getLigneCombatJoueur(this.getJoueurAdverse()).getCartes()[i]!=null){
                             hasCartes=true;
                         }
                    }
                    if(hasCartes){
                        this.phaseDefense();
                    }
                    //on passe de défense à invocation
                    numPhase++;
                    System.out.println("num de phase : " + getNumPhase());
                    pha=Phase.values()[this.getNumPhase()-1];
                    setPhaseActive(pha);  
                    notifyObserver();
                    break;

                case invocation:                   
                    this.phaseInvocation();
                    //on passe de invocation à Attaque
                    numPhase++;
                    System.out.println("num de phase : " + getNumPhase());
                    pha=Phase.values()[this.getNumPhase()-1];
                    setPhaseActive(pha);
                    notifyObserver();

                    break;
                    
                case attaque:
                    this.phaseAttaque();
                    // On passe de attaque à NEW TOUR + on passe à défense
                    //tour nouveau joueur ++ on recommence à defense
                    setNumPhase(0);
                    System.out.println("num de phase : " + getNumPhase());
                    pha=Phase.values()[this.getNumPhase()-1];
                    setPhaseActive(pha);
                    nextJActif();
                    System.out.println("PSEUDOOOO : " + this.joueurActif.getPseudo());
                    notifyObservers();
                    break;
                
                default:      
                    notifyObserver();
                    break;
            }
        
        System.out.println("Phase :" + getPhaseActive());      
        notifyObserver();
    }
    
    
    /**
     * Phase d'invocation du joueur actif
     */
    public void phaseInvocation(){
        // Géré par le contrôleur 
        //On augmente la ressource max, et on remplie le joueur en ressource pour le debut de son tour       
        //On passe à la phase suivante quand le joueur clic sur le bouton
         //enleve la fatigue des cartes du joueur actif (celles qui étaient en état de fatigue, et que le joueur n'a pas pu utiliser ce tour)
        for(int j=0; j<this.getPlateau().getTerrainJoueur(this.getJoueurAdverse()).getCartes().length;j++){
            if(plateau.getTerrainJoueur(this.getJoueurAdverse()).getCartes()[j] instanceof Creature){
                plateau.getTerrainJoueur(this.getJoueurAdverse()).getCartes()[j].setEstFatigue(false);
            } 
        }
        setChanged();
        notifyObserver();
    }
    
    /**
     * Phase d'Attaque du joueur actif
     */
    public void phaseAttaque(){
        for(int i=0; i<plateau.getTaille();i++){
            if(plateau.getLigneCombatJoueur(this.getJoueurActif()).getCartes()[i]!=null){
                // O = joueur Actif, dans tous les cas seul le joueur actif peut faire 
                // la phase d'attaque, on vérifie qu'il a au moins une carte 
                // sur la ligne d'attaque
            
                   this.plateau.getLigneCombatJoueur(this.getJoueurActif()).getCartes()[i].setEstFatigue(true);
                    System.out.println("!!!!!!!!!!!!!!!!!!!!on passe la");
//                   System.out.println("est fatigué ? : "+this.plateau.getLigneCombatJoueur(this.getJoueurActif()).getCartes()[i].estFatigue);
                }
 //            System.out.println("Une carte ? :" + plateau.getLigneCombatJoueur(this.getJoueurActif()).getCartes()[i]);  
            }
       
        
       
        //On passe à la phase suivante quand le joueur clic sur le bouton
        setChanged();
        notifyObservers();
    }
    
    /**
     * Phase de défense du joueur Actif
     */
    public void phaseDefense(){
        System.out.println("ON PASSE DANS DEFENSE");
        /*
        getJoueurActif().setRessourceMax(getJoueurActif().getRessourceMax()+1);
        getJoueurActif().setRessource(getJoueurActif().getRessourceMax());
        */
        System.out.println("PDV JOUEUR actif début: " + getJoueurActif().getPDV());
        
       
        
        //Le joueur actif place ses cartes en défense
       
        
        //Gestion des combats, du retour des cartes et de la fatigue

        for(int i=0; i<this.getPlateau().getLigneCombatJoueur(this.getJoueurAdverse()).getCartes().length; i++){
            if(plateau.getLigneCombatJoueur(this.getJoueurActif()).getCartes()[i] instanceof Creature){
                // Si les cartes sont des créatures face à face ==> Combat
                if(plateau.getLigneCombatJoueur(this.getJoueurActif()).getCartes()[i] instanceof Creature){
                    System.out.println("avant fight");
                    combatCreatures(plateau,(Creature)plateau.getLigneCombatJoueur(this.getJoueurActif()).getCartes()[i],(Creature)plateau.getLigneCombatJoueur(this.getJoueurAdverse()).getCartes()[i]);
                    System.out.println("après fight");
                    //Après le combat, on enleve les cartes de la ligne de combat
                    this.getPlateau().getLigneCombatJoueur(this.getJoueurActif()).getCartes()[i]=null;   
                    this.getPlateau().getLigneCombatJoueur(this.getJoueurAdverse()).getCartes()[i]=null; 
                    setChanged();
                    notifyObservers();
                }
                 System.out.println("je suis la ");
            }
            //Si l'adversaire à une créature et que nous n'avons pas de défense ==> Attaque Directe
            else{ 
                    System.out.println("tu vois ?");
                    System.out.println("carte adverse :" + (Creature)plateau.getLigneCombatJoueur(this.getJoueurAdverse()).getCartes()[i]);
                if((Creature)plateau.getLigneCombatJoueur(this.getJoueurAdverse()).getCartes()[i] != null){
                    System.out.println("tu prend cher");
                    attaqueDirecte(plateau,(Creature)plateau.getLigneCombatJoueur(this.getJoueurAdverse()).getCartes()[i]); 
                    this.getPlateau().getTerrainJoueur(this.getJoueurAdverse()).ajouterCarte(i,(Creature)plateau.getLigneCombatJoueur(this.getJoueurAdverse()).getCartes()[i]);  
                    this.getPlateau().getLigneCombatJoueur(this.getJoueurAdverse()).supprimerCarte(i);  
                    setChanged();
                    notifyObservers();
                }
                
            }
        }
        
        
        //Verifie si le joueur actif a perdu        
        if(getJoueurActif().getPDV()<=0){
            //Le joueur actif à perdu la partie
        }
        System.out.println("PDV JOUEUR actif fin: " + getJoueurActif().getPDV());
        //On passe à la phase suivante quand le joueur clic sur le bouton
        setChanged();
        notifyObservers();
    }
    

    /**
     * @return the joueurActif
     */
    public Joueur getJoueurActif() {
        return joueurActif;
    }

    /**
     * @return the listeJoueur
     */
    public Joueur[] getListeJoueur() {
        return listeJoueur;
    }

    /**
     * @return the joueurAdverse
     */
    public Joueur getJoueurAdverse() {
        return joueurAdverse;
    }

    /**
     * @return the nbTour
     */
    public int getNbTour() {
        return nbTour;
    }

    /**
     * @return the partieFinie
     */
    public boolean isPartieFinie() {
        return partieFinie;
    }

    /**
     * @return the phaseActive
     */
    public Phase getPhaseActive() {
        return phaseActive;
    }

    /**
     * @return the numPhase
     */
    public int getNumPhase() {
        return numPhase+1;
    }
    
    /**
     * Change le joueur actif
     */
    public void nextJActif(){
        
        System.out.println("COUCOU");
        
        
        if(listeJoueur[0].getPseudo().toString()==joueurActif.getPseudo().toString()){
            this.joueurActif = listeJoueur[1];
            this.joueurAdverse = listeJoueur[0];
        }
        else{
            this.joueurActif=listeJoueur[0];
            this.joueurAdverse = listeJoueur[1];
        }
        setChanged();
        notifyObservers(this.joueurActif);
        
    }

    /**
     * @param nbTour the nbTour to set
     */
    public void setNbTour(int nbTour) {
        this.nbTour = nbTour;
        setChanged();
        notifyObservers(this.nbTour);
    }

    /**
     * @param numPhase the numPhase to set
     */
    public void setNumPhase(int numPhase) {
        this.numPhase = numPhase;
        setChanged();
        notifyObservers(this.numPhase);
    }
    
    
     public Joueur getJ1()
    {
        return this.listeJoueur[0];
    }
    
    public Joueur getJ2()
    {
        return this.listeJoueur[1];
    }
    
    @Override
    public void addObserver(Observer obs) {

        this.listObserver.add(obs);

    }
    
    public void notifyObserver(){
      for(Observer obs : this.listObserver)
      obs.update(this, null);
    };

    /**
     * @param phaseActive the phaseActive to set
     */
    public void setPhaseActive(Phase phaseActive) {
        this.phaseActive = phaseActive;
    }
    
    
    public void removeObserver(){
        listObserver = new ArrayList<>();
    }
}