
package magic.modele;

/**
 *
 * @author P1302812 & P1509371
 */
public class LigneCombat extends LigneCartes
{
    /**
     * Joueur à qui appartient la ligne de combat
     */
    protected Joueur joueur;
    /**
     * Cartes placées sur la ligne de combat
     */
    protected Carte[] cartes;
    
    protected Plateau plat;
    
    /**
     * Constructeur de la ligne de combat
     * @param j Joueur à qui appartient la ligne de combat
     * @param p Plateau de jeu
     */
    public LigneCombat(Joueur j, Plateau p)
    {
        super(j,p);
        this.joueur = j;
        this.cartes = new Carte[p.getTaille()];
        this.plat = p;
    }
    
    /**
     * Récupere le joueur à qui appartient la ligne de combat
     * @return Joueur
     */
    public Joueur getJoueur()
    {
        return this.joueur;
    }
    
    /**
     * On récupère la carte placée sur la ligne de combat et à la colonne posY
     * @param posY la colonne de la carte de la ligne de ocmbat
     * @return Carte sur la ligne de combat et à la colonne posY
     */
    public Carte getCarte(int posY)
    {
        return this.cartes[posY];
    }
    
    /**
     * Liste des cartes de la ligne de Combat
     * @return Liste Cartes placées sur ligne de Combat
     */
    public Carte[] getCartes()
    {
        return this.cartes;
    }
    
    public Creature[] getCreatures()
    {
        return (Creature[])this.cartes;
    }
    
    /**
     * Remplacer une ligne de combat par de nouvelles cartes 
     * @param nouvellesCartes 
     */
    @Override
    public boolean setCartes(Carte[] nouvellesCartes)
    {
        this.cartes = nouvellesCartes;
        super.notifyObserverArray(nouvellesCartes);
        return true;
    }
    
    /**
     * Supprimer une carte de la ligne de combat et placée à la colonne posY
     * @param posY colonne de la carte à supprimer
     */
    public void supprimerCarte(int posY)
    {
        this.cartes[posY] = null;
        super.notifyObserver(this.cartes[posY]);
    }
    
    /**
     * Ajouter une carte sur la ligne de combat à l'emplacement indiqué par posY
     * @param posY Colonne où ajouter la carte 
     * @param c Carte à ajouter sur la ligne de combat
     * @return booléen, retourne vrai si on peut placer la carte
     */
    @Override
    public boolean ajouterCarte(int posY, Carte c)
    {
        for(int i = 0; i <=  this.cartes.length; i++)
        {
            if(posY==i && this.cartes[i] == null)
            {
                this.cartes[i] = c;
                super.notifyObserver(c);
                return true;
            }
        }
        return false;
    }
    
   /**
    * On place les cartes dans le tableau
    * +
    * A utiliser pour replacer les cartes dans le plateau
    */
   public void updateLC()
   {
       for(int i = 0; i < this.plat.getTaille(); i ++)
       {
           if(this.getCartes()[i]!=null){
               this.getCartes()[i].toString();
           }
           else{
               this.cartes[i] = null;
           }
       }
   }
}
