
package magic.modele;

/**
 * Une classe abstraite permettant de généraliser les capacités des cartes
 * @author P1302812 & P1509371
 */
public abstract class DecorateurCarte extends Carte {
    
    /**
     * Constructeur de DecorateurCarte
     * @param c Carte "à décorer" / à laquelle on va appliquer un effet
     */
    public DecorateurCarte(Carte c){
        super(c.posX, c.posY);
    }
}
