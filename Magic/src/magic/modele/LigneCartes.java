
package magic.modele;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Marc
 */
public abstract class LigneCartes extends Observable{
    
    /**
     * Joueur à qui appartient la ligne de cartes
     */
    protected Joueur joueur;
    /**
     * Cartes placées sur la ligne de cartes
     */
    protected Carte[] cartes;
    protected ArrayList<Observer> listeObserver = new ArrayList<Observer>();
    protected Plateau plt;

    public LigneCartes(Joueur j, Plateau p) {
        this.plt=p;
        this.joueur=j;
    }
    
    /**
     * Récupere le joueur à qui appartient la ligne de cartes
     * @return Joueur
     */
    public Joueur getJoueur()
    {
        return this.joueur;
    }
    
    /**
     * On récupère la carte placée sur la ligne de cartes et à la colonne posY
     * @param posY la colonne de la carte de la ligne de cartes
     * @return Carte sur la ligne de combat et à la colonne posY
     */
    public Carte getCarte(int posY)
    {
        return this.cartes[posY];
    }
      
    
    /**
     * Liste des cartes de la ligne de cartes
     * @return Liste Cartes placées sur ligne de cartes
     */
    public Carte[] getCartes()
    {
        return this.cartes;
    }
    
      
    /**
     * Remplacer une ligne de cartes par de nouvelles cartes 
     * @param nouvellesCartes 
     * @return Vrai : les cartes ont été remplacée par de nouvelles
     */
    public boolean setCartes(Carte[] nouvellesCartes)
    {   
        this.cartes = nouvellesCartes;
        notifyObserverArray(nouvellesCartes);
        return true;
    }
    

    /**
     * Supprimer une carte de la ligne de cartes et placée à la colonne posY
     * @param posY colonne de la carte à supprimer
     */
    public void supprimerCarte(int posY)
    {
        this.cartes[posY] = null;
        notifyObserver(this.cartes[posY]);
    }
    
        
    
    /**
     * Ajouter une carte sur la ligne de cartes à l'emplacement indiqué par posY
     * @param posY Colonne où ajouter la carte 
     * @param c Carte à ajouter sur la ligne de cartes
     * @return booléen, retourne Vrai si on peut ajouter une carte
     */
    public boolean ajouterCarte(int posY, Carte c)
    {
        for(int i = 0; i <=  this.cartes.length; i++)
        {
            if(this.cartes[i] == null && posY==i)
            {
                this.cartes[posY] = c;
                notifyObserver(c);
                return true;
            }
        }
        return false;
    }
    
    public void addObserver(Observer obs) {

        this.listeObserver.add(obs);

    }
    
    public void notifyObserver(Carte str){
      for(Observer obs : this.listeObserver)
      obs.update(this, str);
    };
    
    public void notifyObserverArray(Carte[] str){
      for(Observer obs : this.listeObserver)
      obs.update(this, str);
    };

}
