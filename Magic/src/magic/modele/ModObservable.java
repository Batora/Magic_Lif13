/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package magic.modele;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author romain
 */
public class ModObservable extends Observable {
    
    protected ArrayList<Observer> listObserver = new ArrayList<Observer>();   

    
    @Override
    public void addObserver(Observer obs) {

    this.listObserver.add(obs);

    }
    
    public void notifyObserver(){};
    
    
}
