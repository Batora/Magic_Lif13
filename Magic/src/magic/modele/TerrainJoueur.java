
package magic.modele;

/**
 *
 * @author P1302812 & P1509371
 */
public class TerrainJoueur extends LigneCartes
{
    /**
     * Le joueur à qui appartient le terrain
     */
    protected Joueur joueur;
    
    /**
     * Les cartes placée sur le terrain
     */
    protected Carte[] cartes;
    private int posCarteDeplacement = -1;
    protected Plateau plat;
    
    /**
     * Constructeur du terrain du joueur
     * @param j Joueur à qui appartient le terrain
     * @param p Plateau de jeu
     */
    public TerrainJoueur(Joueur j, Plateau p)
    {
        super(j,p);
        this.joueur = j;
        this.plat = p;
        this.cartes = new Carte[p.getTaille()];
    }
    
    /**
     * Récupere le joueur à qui appartient le terrain
     * @return Joueur
     */
    public Joueur getJoueur()
    {
        return this.joueur;
    }
    
    /**
     * On récupère la carte placée sur le terrain et à la colonne posY
     * @param posY  la colonne de la carte du terrain
     * @return Carte sur le terrain et à la colonne posY
     */
    public Carte getCarte(int posY)
    {
        return this.cartes[posY];
    }
    
    /**
     * Liste des cartes du terrain 
     * @return Liste Cartes placées sur le terrain
     */
    public Carte[] getCartes()
    {
        return this.cartes;
    }
    
    /**
     * Remplacer un terrain par de nouvelles cartes 
     * @param nouvellesCartes 
     */
    public boolean setCartes(Carte[] nouvellesCartes)
    {
        this.cartes = nouvellesCartes;
        super.notifyObserverArray(nouvellesCartes);
        return true;
    }
    
    /**
     * Supprimer une carte du terrain et placée à la colonne posY
     * @param posY colonne de la carte à supprimer
     */
    public void supprimerCarte(int posY)
    {
        this.cartes[posY] = null;
        super.notifyObserver(this.cartes[posY]);
    }
    
    /**
     * Ajouter une carte sur le terrain à l'emplacement indiqué par posY
     * @param posY Colonne où ajouter la carte 
     * @param c Carte à ajouter sur la ligne de combat
     * @return booléen vrai si on peut ajouter une carte
     */
    @Override
    public boolean ajouterCarte(int posY, Carte c)
    {
        for(int i = 0; i <=  this.cartes.length; i++)
        {
            if(this.cartes[posY] == null && posY==i)
            {
                this.cartes[i] = c;
                super.notifyObserver(c);
                return true;
            }
        }
        return false;
    }
    
    
   
    
    
   /**
    * On place les cartes dans le tableau
    * +
    * A utiliser pour replacer les cartes dans le plateau
    */
   public void updateTerrain()
   {
       for(int i = 0; i < this.plat.getTaille(); i ++)
       {    
           if(this.getCartes()[i]!=null){
               this.getCartes()[i].toString();
           }
           else{
               this.cartes[i] = null;
           }
       }
   }
   
   /**
    * On va déclarer cette carte comme attaquante
    * @param posTerrain
    * @return booléen : Vrai si la carte est attaquante, faux sinon
    */
    public boolean initCarteAtk(int posTerrain)
    {
        // On s'assure d'avoir une carte de type Créature
        Creature c = (Creature)this.getCarte(posTerrain);

        if(c != null)
        {
            // On vérifie que cette carte n'est pas fatiguée
            if(c.isEstFatigue()== false)
            {
                // On ajoute cette carte sur la ligne de combat
                if(super.plt.getLigneCombatJoueur(this.getJoueur()).ajouterCarte(posTerrain,c))
                {
                    // On supprime cette carte du terrain
                    this.supprimerCarte(posTerrain);
                    // on passe cette carte dans l'état de fatigue
                    c.setEstFatigue(true);
                    super.notifyObserver(null);
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * On va gérer la défense ici, si une carte est en attaque
     * @param posDefenseur la position de la carte pour la défense
     * @param posAttaquant la position de la carte attaquante
     * @param LCA la ligne de combat
     * @return booléen : Vrai si la carte repasse à sa position sur le terrain, 
     * faux si les cartes n'existent pas
     */
    public boolean initCarteDef(int posDefenseur, int posAttaquant, LigneCombat LCA)
    {
        // On s'assure d'avoir une carte de type Créature
        Creature defenseur = (Creature)this.getCarte(posDefenseur);
        Carte attaquant = (Creature)LCA.getCarte(posAttaquant);

        // On vérifie que les cartes existent
        if(attaquant == null && defenseur != null)
        {
            if(defenseur.isEstFatigue()== false)
            {
                // On ajoute la carte sur la ligne de combat
                if(super.plt.getLigneCombatJoueur(this.joueur).ajouterCarte(posAttaquant,defenseur))
                {
                    // On supprime la carte du terrain
                    this.supprimerCarte(posDefenseur);
                    // on passe la carte dans l'état de fatigue
                    defenseur.setEstFatigue(true);

                    return true;
                }
            }
        }

        return false;
    }
    
    /**
     * Récupérer la position du déplacement
     * @return la position du déplacement d'une carte
     */
    public int getDeplacement() {
        return posCarteDeplacement;
    }
    
    /**
     * Retourne la position de la carte qui va être déplacée
     * @param c carte à déplacer
     * @return position de la carte à déplacer
     */
    public int getPosCarteDeplacement(Carte c)
    {
        for (int i = 0; i <4; i++){
            if (this.cartes[i].getNom() == c.getNom()){
                return i;
            }
        }
        return 42;
    }
   
    /**
     * On va donner la position de la carte à déplacer
     * @param pos Position de la carte à déplacer
     */
    public void setPosCarteDeplacement(int pos)
    {
        this.posCarteDeplacement = pos;
        
    }
       
}
