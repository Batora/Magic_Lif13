package magic.modele;

/**
 * Carte de type créature, définie par 
 * une valeur d'attaque, de défense, de coût en ressources et d'un nom
 * @author P1302812 & P1509371
 */
public class Creature extends Carte {

    /**
     * Valeur de l'attaque d'une carte
     */
    private int atk;

    /**
     * Valeur de la défense d'une carte
     */
    private int def;

    /**
     * Valeur de la ressource à dépenser pour pouvoir invoquer la carte
     */
    private int cout;



    /**
     * Constructeur de Créature
     * @param posX
     * @param posY
     * @param atk
     * @param def
     * @param cout
     * @param nom
     */
    public Creature(int posX, int posY, int atk, int def, int cout, String nom){
        super(posX, posY);
        this.atk=atk;
        this.def=def;
        this.cout=cout;
        this.nom=nom;
    }
    
    /**
     * Affichage d'une carte --> Pour le débug
     */
    public void afficherCarte(){
        System.out.println(toString());
    }
    
    @Override
    public String toString(){
        if(this != null){
            return this.getNom()+","+this.getCout()+","+this.getAtk()+","+this.getDef() + "||";  
        }
        else{
            return "null";
        }
    }

    /**
     * @return the atk
     */
    public int getAtk() {
        return atk;
    }

    /**
     * @return the def
     */
    public int getDef() {
        return def;
    }

    /**
     * @return the cout
     */
    public int getCout() {
        return cout;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }
}
