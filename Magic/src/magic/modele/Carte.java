package magic.modele;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Classe abstraite servant à définir une carte au sens général
 * avec une position X et Y sur le plateau, l'attribut fatigué 
 * sert à s'assurer qu'une carte ne sera pas jouée 2 tours de suite.
 * @author P1302812 & P1509371
 */
public abstract class Carte  extends Observable {

    /**
     * Ligne sur laquelle la carte se trouve
     */
    protected int posX;

    /**
     * Colonne sur laquelle la carte se trouve 
     */
    protected int posY;

    /**
     * Ligne de départ de la carte dans la main du joueur
     */
    protected int posXDepart;

    /**
     * Colonne de départ de la carte dans la main du joueur
     */
    protected int posYDepart;

    /**
     * Informe sur l'état de la carte, si elle est en mode "reposée", 
     * estFatigue=false, on va pouvoir jouer la carte ce tour;
     * Sinon elle est "fatiguée", on ne peut pas jouer cette carte ce tour.
     */
    protected boolean estFatigue;
    
    /**
    * Nom de la carte
    */
    protected String nom;
    
    protected ArrayList<Observer> listeObserver = new ArrayList<Observer>(); 
    
    /**
     * Constructeur de Carte
     * @param posXDepart
     * @param posYDepart
     */
    public Carte(int posXDepart, int posYDepart){
        this.posXDepart=posXDepart;
        this.posYDepart=posYDepart;              
    }

    
    /**
     * @return the posX
     */
    public int getPosX() {
        return posX;
    }

    /**
     * @param posX the posX to set
     */
    public void setPosX(int posX) {
        this.posX = posX;
    }

    /**
     * @return the posY
     */
    public int getPosY() {
        return posY;
    }

    /**
     * @param posY the posY to set
     */
    public void setPosY(int posY) {
        this.posY = posY;
    }

    /**
     * @return Position X de départ
     */
    public int getPosXDepart() {
        return posXDepart;
    }

    /**
     * @param posXDepart La position X de départ to set
     */
    public void setPosXDepart(int posXDepart) {
        this.posXDepart = posXDepart;
    }

    /**
     * @return the posYDepart
     */
    public int getPosYDepart() {
        return posYDepart;
    }

    /**
     * @param posYDepart the posYDepart to set
     */
    public void setPosYDepart(int posYDepart) {
        this.posYDepart = posYDepart;
    }

    /**
     * La carte est-elle fatiguée ? = Peut-elle être utilisée ce tour ?
     * @return Booleen : OUI ou NON
     */
    public boolean isEstFatigue() {
        return estFatigue;
    }

    /**
     * @param estFatigue the estFatigue to set
     */
    public void setEstFatigue(boolean estFatigue) {
        this.estFatigue = estFatigue;
    }
    
    
    @Override
    public String toString(){
        if(this == null){
            return "[0]";
        }
        else{
            return "";
        }
    }

    public String getNom() {
        return this.nom;
    }
    
    public void setNom(String nomCarte){
        this.nom=nomCarte;
    }
    
      
    //Implémentation du pattern observer
    public void addObserver(Observer obs) {
      this.listeObserver.add(obs);
    }

    public void updateObserver(Carte c) {
      for (Observer obs : this.listeObserver){
          obs.update(this, c);
      }
    }

    public void removeObserver() {
      listeObserver = new ArrayList<Observer>();
    }  
}
