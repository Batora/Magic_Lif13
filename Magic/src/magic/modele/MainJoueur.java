
package magic.modele;

/**
 *
 * @author P1302812 & P1509371
 */
public class MainJoueur extends LigneCartes
{
    /**
     * taille d'une pioche de joueur, par défaut : 4
     */
    protected int tailleMax = 4;
    
    /**
     * Le deck des joueurs
     * /!\ Le deck des joueurs comprend TOUTES les cartes du joueur, mais seule
     * les t premières cartes avec t étant la tailleMax s'afficheront sur le plateau
     * les autres seront considérés comme dans la pioche/deck du joueur
     */
    protected Carte[] cartes;
    protected Plateau plt;
    private Joueur joueur;
    
    
    /**
     * Constructeur 1 de Main Joueur
     * @param j On passe en paramètre le Joueur concerné
     * @param p Plateau de jeu
     */
    public MainJoueur(Joueur j, Plateau p)
    {
        super(j,p);
        this.plt=p;
        this.joueur = j;
        this.cartes = new Carte[tailleMax];
    }
    
    /**
     * Constructeur 2 de Main Joueur
     * @param j On passe en paramètre le joueur concerné
     * @param t la taille de la pioche du joueur
     */
    public MainJoueur(Joueur j, Plateau p, int t)
    {
        super(j,p);
        this.tailleMax=t;
        this.joueur = j;
        this.cartes = new Carte[tailleMax];
    }
    
    /**
     * récupérer la taille d'une main/pioche de joueur
     * @return 
     */
    public int getTailleMax()
    {
        return this.tailleMax;
    }
    
    /**
     * Récupérer la carte qui se trouve à un endroit posY de la main
     * @param posY La colonne d'une carte <=> son posY
     * @return Carte qui se trouve à la colonne posY
     */
    public Carte getCarte(int posY)
    {
        return this.cartes[posY];
    }
    
    /**
     * Récupérer la liste des cartes de la main
     * @return 
     */
    public Carte[] getCartes()
    {
        return this.cartes;
    }
    
    /**
     * On va placer de nouvelles cartes dans la main du joueur : pioche de cartes
     * @param nouvellesCartes Une nouvelle pioche (complète)
     */   
    public boolean setCartes(Carte[] nouvellesCartes)
    {
        this.cartes = nouvellesCartes;
        super.notifyObserverArray(nouvellesCartes);
        return true;
    }
    
    /**
     * On va supprimer la carte de la main à la colonne posY
     * @param posY la colonne de la main où se trouve la carte à supprimer
     */
    public void supprimerCarte(int posY)
    {
        this.cartes[posY] = null;
        super.notifyObserver(this.cartes[posY]);
        // Décaler cartes
    }
    
    /**
     * On ajoute une carte à la main du joueur
     * @param posY Colonne où ajouter la carte
     * @param c Carte à ajouter à la main d'un joueur
     */
    @Override
    public boolean ajouterCarte(int posY, Carte c)
    {
        for(int i = 0; i <=  this.cartes.length; i++)
        {
            if(this.cartes[i] == null && posY==i)
            {
                this.cartes[posY] = c;
                super.notifyObserver(c);
                return true;
            }
        }
        return false;
    }
    
    /**
     * On retourne le joueur qui possède la main
     * @return Joueur à qui appartient la main
     */
    public Joueur getJoueur()
    {
        return this.joueur;
    }
    
 
    /**
     * On place les cartes dans le tableau
     * +
     * A utiliser pour replacer les cartes dans le plateau
     */
   public void updateMain()
   {    
       for(int i=0;i<this.tailleMax;i++){
           if(this.getCartes()[i]!=null){
               this.getCartes()[i].toString();
           }else{
               this.getCartes()[i]=null;
           }
       }
   }       

}
