
package magic.modele;

/**
 * Création d'un objet Phase qui énumère toutes les phases possibles 
 * d'un tour de jeu.
 * Cela va empêcher de passer un paramètre inattendu à une méthode.
 * @author P1302812 & P1509371
 */
public enum Phase {

    defense ("défense !"),
    invocation ("invocation !"),
    attaque ("attaque !");


  private String name = "";
   
  Phase(String name){
    this.name = name;
  }
   
    @Override
  public String toString(){
    return name;
  }
}