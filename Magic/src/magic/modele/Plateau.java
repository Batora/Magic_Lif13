
package magic.modele;

/**
 * Plateau de jeu, va contenir les cartes des joueurs
 * @author P1302812 & P1509371
 */
public class Plateau {
    
    /**
     * taille du plateau : Les colonnes du terrain et des lignes de combat des 
     * joueurs
     */
    protected int taille;
    
    /**
     * Tableau contenant les 2 mains des joueurs
     */
    protected MainJoueur[] mains;
    
    /**
     *  Tableau contenant les 2 terrains des joueurs
    */
    protected TerrainJoueur[] terrains;
    
    /**
     * tableau contenant les 2 lignes des combats des joueurs
     */
    protected LigneCombat[] lignesCombat;
    
    /**
     * Constructeur de Plateau
     */
    public Plateau(){
        this.taille = 4;
        this.mains = new MainJoueur[2];
        this.terrains = new TerrainJoueur[2];
        this.lignesCombat = new LigneCombat[2];
    }
    
     /**
     * Constructeur de Plateau avec possibilité de modifier la taille 
     * @param t taille des terrains et des Lignes de Combat des joueurs
     */
    public Plateau(int t){
        this.taille = t;
        this.mains = new MainJoueur[2];
        this.terrains = new TerrainJoueur[2];
        this.lignesCombat = new LigneCombat[2];
    }
    
    
    /**
     * Récupérer la main du joueur passé en paramètre
     * @param j Joueur passé en paramètre
     * @return Liste des cartes possédées par le joueur dans sa main
     */    
    public MainJoueur getMainJoueur(Joueur j)
    {
        MainJoueur mj = null;
        
        for(int i = 0; i < 2; i++)
        {
            if(this.mains[i].getJoueur() == j)
            {
                mj = this.mains[i];
            }
        }
        
        return mj;
    }
    
    /**
     * Récupérer le terrain d'un joueur passé en paramètres
     * @param j Joueur passé en paramètre
     * @return Liste des cartes présentes sur le Terrain d'un joueur
     */
    public TerrainJoueur getTerrainJoueur(Joueur j)
    {
        TerrainJoueur tj = null;
        
        for(int i = 0; i < 2; i++)
        {
            if(this.terrains[i].getJoueur() == j)
            {
                tj = this.terrains[i];
            }
        }
        
        return tj;
    }
    
    /**
     * Récuoérer la ligne de Combat d'un joueur passé en paramètres
     * @param j Joueur passé en paramètre
     * @return Liste des cartes présentes sur la ligne de combat d'un joueur
     */
    public LigneCombat getLigneCombatJoueur(Joueur j)
    {
        LigneCombat lc = null;
        
        for(int i = 0; i < 2; i++)
        {
            if(this.lignesCombat[i].getJoueur() == j)
            {
                lc = this.lignesCombat[i];
            }
        }
        
        return lc;
    }
    
    
    /**
     * Initialisation du plateau
     * @param joueurs liste des joueurs actifs
     */
    public void initialiser(Joueur[] joueurs)
    {
        for(int i = 0; i < 2; i++)
        {
                this.mains[i] = new MainJoueur(joueurs[i],this);
                this.terrains[i] = new TerrainJoueur(joueurs[i], this);
                this.lignesCombat[i] = new LigneCombat(joueurs[i], this);
            
        }
    }
    
    
    /**
     * Poser une carte à un emplacement du plateau
     * @param c Carte à poser sur le plateau
     * @param id Numéro du joueur dans le tableau
     */
    public void poserCarte(Carte c, int id){
        if(c.getPosX() == 0){
            if(mains[id].getCartes()[c.getPosY()]==null){
                // on s'assure que l'emplacement soit vide
                mains[id].ajouterCarte(id, c);
            }            
        }
        if(c.getPosX() == 1){
            if(terrains[id].getCartes()[c.getPosY()]==null){
                // on s'assure que l'emplacement soit vide
                terrains[id].ajouterCarte(id, c);
            }            
        }
          if(c.getPosX() == 2){
            if(lignesCombat[id].getCartes()[c.getPosY()]==null){
                // on s'assure que l'emplacement soit vide
                lignesCombat[id].ajouterCarte(id, c);
            }            
        }
            
    }
    
    /**
     * Un simple affichage en mode console
     */
    public void afficherPlateau(){ 
        System.out.println(this);
    }
    
    @Override
    public String toString(){
        String res="";
        for(int i=0; i<2; i++){
            //Parcours du côté de l'un des joueurs
            for(int k=0; k<this.mains[i].getTailleMax();k++){
                res+=" | main j" +i+": "+ mains[i].getJoueur().getJmain().getCartes()[k] + " |"; 
            }            
            res+="\n";
            for(int j=0;j<this.getTaille();j++){
                res+=" | terrain j"+i+"      : " + terrains[i].getCartes()[j] + " |";
            }
            res+="\n";
            for(int l=0;l<this.getTaille();l++){
                res+=" | ligne combat : j"+ i + " " + lignesCombat[i].getCartes()[l] +  " |";   
            }
            res+="\n \n";
           
        }
        return res;
        
    }  


    /**
     * @return the taille
     */
    public int getTaille() {
        return this.taille;
    }

    /**
     * @param taille the taille to set
     */
    public void setTaille(int taille) {
        this.taille = taille;
    }

    /**
     * @return the mains
     */
    public MainJoueur[] getMains() {
        return this.mains;
    }
    
        /**
     * @return the mains
     */
    public MainJoueur getMain(int joueur) {
        return this.mains[joueur];
    }
    
    /**
     * @return the terrains
     */
    public TerrainJoueur[] getTerrains() {
        return terrains;
    }

    /**
     * @return the lignesCombat
     */
    public LigneCombat[] getLignesCombat() {
        return lignesCombat;
    }
}
