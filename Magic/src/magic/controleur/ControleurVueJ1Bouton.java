
package magic.controleur;

import java.awt.event.ActionEvent;
import javax.swing.JButton;
import magic.modele.Jeu;
import magic.vue.VueJ1;
import magic.vue.VueJ2;

/**
 *
 * @author Marc
 */
public class ControleurVueJ1Bouton extends Controleur{
        
    VueJ1 vue;
    Jeu modele;
    

    public ControleurVueJ1Bouton(VueJ1 vue, Jeu modele) {
        this.vue = vue;
        this.modele = modele;
    }

    public void actionPerformed(ActionEvent e) {
        
       String src = ((JButton)e.getSource()).getText();
   
        if(src=="Phase Suivante" && this.modele.getNumPhase()==2){
            
            
            /** ON CHANGE NUM Phase**/ 
            System.out.println("phase : " + this.modele.getNumPhase());
            this.vue.setsNumPhase(Integer.toString(this.modele.getNumPhase()+1));
            this.vue.getNumPhase().setText("phase " + this.vue.getsNumPhase() + "\n");
            this.vue.getNbTour().setText("Tour : " + this.modele.getNbTour());
            /**
             *  nom phase 
             */
            this.modele.nextPhase();
            this.vue.getNomPhase().setText("(" +this.modele.getPhaseActive().name() + ")");
            
            System.out.println("phase active : " + this.modele.getPhaseActive() );
            ((JButton)e.getSource()).setText("Tour suivant");
            this.vue.revalidate();
            this.vue.repaint();
            
          
            
        }
        else if(src=="Tour suivant" && this.modele.getNumPhase()==3){
             /** ON CHANGE NB TOUR **/ 
            this.modele.nextPhase();
           
            
            /** on met les ressources aux joueurs */
            this.modele.getJoueurActif().setRessourceMax(this.modele.getJoueurActif().getRessourceMax()+1);
            this.modele.getJoueurActif().setRessource(this.modele.getJoueurActif().getRessourceMax());
            
            
            ((JButton)e.getSource()).setText("Phase Suivante");
            System.out.println("phase active : " + this.modele.getPhaseActive() );
            this.vue.revalidate();
            this.vue.repaint();
            this.modele.setNumPhase(0);
            
            String ress=Integer.toString(this.modele.getJoueurActif().getRessource());
            String ressMax = Integer.toString(this.modele.getJoueurActif().getRessourceMax());
                                    
            this.vue.setsRessources(ress);
            this.vue.getRessourcesJActif().revalidate();
            this.vue.getRessourcesJActif().repaint();
            System.out.println("ress :" + ress);
            this.vue.getRessourcesJActif().setText("Ressources : " + ress + "/" + ressMax );
            
        
             System.out.println("jActif : " + this.modele.getJoueurActif().getPseudo());
            Controleur ctrl = new ControleurVueJoueur2(this.modele);
            
            VueJ2 vueJ2 = new VueJ2(ctrl);
            vueJ2.initComponent();
            vueJ2.setVisible(true);
            this.vue.dispose();
                                    
            
            
           // this.modele.
        }
        else if (src=="Phase Suivante" && this.modele.getNumPhase()<2){
            /** ON CHANGE PHASES **/ 
            ((JButton)e.getSource()).setText("Phase Suivante");
            this.modele.nextPhase();
            /** Numéro de phase **/
            //this.modele.setNumPhase(this.modele.getNumPhase()+1)
            this.vue.setsNumPhase(Integer.toString(this.modele.getNumPhase()));
            this.vue.getNumPhase().setText("phase " + this.vue.getsNumPhase() + "\n");
        
          
             /** Nom de phase **/
            
            this.vue.getNomPhase().setText("(" + this.modele.getPhaseActive().name() + ")");
            //this.modele.setNumPhase(this.modele.getNumPhase());
            
            System.out.println("phase active : " + this.modele.getPhaseActive() );
            System.out.println("phase suivante");
            this.vue.revalidate();
            this.vue.repaint();
            
        }
        
    }
    
        
        
}
