
package magic.controleur;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.MouseInputAdapter;
import magic.modele.Creature;
import magic.modele.Jeu;
import magic.modele.Phase;
import magic.vue.VueCarteCreature;
import magic.vue.VueJ1;

/**
 *
 * @author Marc
 */
public class ControleurActionsSourisVueJ1 implements MouseListener{
    
    private int posY;
    private int posX;
    protected VueJ1 view;
    protected Jeu modele;
    private VueCarteCreature carteStockee;

    public ControleurActionsSourisVueJ1(Jeu modele, VueJ1 vue, int i, int j) {
        this.posX=i;
        this.posY=j;
        this.view = vue;
        this.modele=modele;
        this.carteStockee=null;
    }
    

    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("clic ! sur ");
        System.out.println("position :  " + this.getPosX() + " y : " + this.getPosY());
        
        if(this.getPosX()==0){
            if(this.modele.getPhaseActive()==Phase.invocation){
                /** si l'évenement capté est dans la main du joueur actif **/
               if(e.getSource() instanceof JPanel){
                   JPanel panelClique=(JPanel)e.getSource();

                    System.out.println("true : 1");
                    /** on s'assure que la carte sur laquelle on clique est bien de type 
                     * VueCarteCreature
                     **/
                            /** Si à la case 0 du tableau des composants dans le JPanel on trouve un objet de type VueCarteCreature **/ 

                                VueCarteCreature carte = (VueCarteCreature)panelClique.getComponents()[0];
                                System.out.println("true : 2-1");

                                /** on récupère la carte **/
                                Creature crea = carte.getCarte();
                                if(crea.getCout()<=this.modele.getJoueurActif().getRessource()){
                                    this.modele.getJoueurActif().setRessource(this.modele.getJoueurActif().getRessource()-crea.getCout());
                                    String ress=Integer.toString(this.modele.getJoueurActif().getRessource());
                                    
                                    
                                    this.view.setsRessources(ress);
                                    this.view.getRessourcesJActif().revalidate();
                                    this.view.getRessourcesJActif().repaint();
                                    System.out.println("ress :" + ress);
                                    this.view.getRessourcesJActif().setText("Ressources : " + ress + "/" + this.modele.getJoueurActif().getRessourceMax() );
                                    
                                    crea.setPosXDepart(getPosX());
                                    crea.setPosYDepart(getPosY());

                                    crea.setPosX(getPosX()+1);
                                    crea.afficherCarte();


                                    this.modele.getPlateau().getTerrainJoueur(this.modele.getJoueurActif()).ajouterCarte(this.getPosY(), crea);                        
                                    this.modele.getJoueurActif().setJterrain(this.modele.getPlateau().getTerrainJoueur(this.modele.getJoueurActif()));

                                    System.out.println("true : 2-2");


                                    this.modele.getJoueurActif().getJmain().supprimerCarte(getPosY());
                                    System.out.println("true : 2-3");
                                    this.view.supprimerCarteMain(getPosY());
                                    this.modele.getPlateau().getTerrainJoueur(this.modele.getJoueurActif()).updateTerrain();

                                    this.modele.getPlateau().afficherPlateau();
                                 //   this.view.remplirMainJActif(this.modele.getJoueurActif().getJmain(), posX, posY);
                                    this.view.remplirTerrainJActifPanel(carte, getPosY());
                                    this.view.revalidate();
                                    this.view.repaint();
                                    
                                }





                    System.out.println("true : 3");
               }
            }
        }
        if(this.getPosX()==1 && (this.modele.getPhaseActive()==Phase.defense || this.modele.getPhaseActive()==Phase.attaque)){
            /** si l'évenement capté est dans le terrain du joueur actif  : ON VA STOCKER LA CARTE **/
            
            /**
             * Quand on pose une carte sur le terrain on veut que la ligne d'attaque soit écoutable
             * --> créer une méthode dans view pour mettre des listeners et faire un parcours de la dernière ligne
             * 
             */
           if(e.getSource() instanceof JPanel){
               JPanel panelClique=(JPanel)e.getSource();

                System.out.println("true : 1");
                /** on s'assure que la carte sur laquelle on clique est bien de type 
                 * VueCarteCreature
                 **/
                    if(panelClique.getComponentCount()<=1){
                        /** Si à la case 0 du tableau des composants dans le JPanel on trouve un objet de type VueCarteCreature **/ 
                        if(panelClique.getComponents()[0] instanceof VueCarteCreature){
                            
                            VueCarteCreature carte = (VueCarteCreature)panelClique.getComponents()[0];
                            System.out.println("true : 2-1");

                            
                            /*
                                Appel méthode de la vue pour créer des écouteurs sur la ligne de Combat
                            */
                            
                            this.view.creationEvenementSourisLC(this.modele.getPlateau().getTaille());
                            
                            /** on récupère la carte **/
                            
                            /*
                            Creature crea = carte.getCarte();
                            crea.setPosXDepart(getPosX());
                            crea.setPosYDepart(getPosY());

                            crea.setPosX(getPosX()+1);
                            crea.afficherCarte();
                             */

                            /*this.modele.getPlateau().getLigneCombatJoueur(this.modele.getJoueurActif()).ajouterCarte(this.getPosY(), crea);                        
                           
                            this.modele.getJoueurActif().setJligneCombat(this.modele.getPlateau().getLigneCombatJoueur(this.modele.getJoueurActif()));
                            */

                            System.out.println("true : 2-2");
                            this.view.setCarteStockee(carte);
                            

                        }
                }
           }
           
        }
        
        if(this.getPosX()==2){
            /**  On va récupérer la carte stockée dans le terrain et on clique sur la Ligne de Combat **/
            System.out.println("clic pan3");
            /** si l'évenement capté est dans le terrain du joueur actif **/
                if(this.view.getCarteStockee()!=null){

                    if(e.getSource() instanceof JPanel){
                        JPanel panelClique=(JPanel)e.getSource();

                         System.out.println("true : 1");
                         /** on s'assure que la carte sur laquelle on clique est bien de type 
                          * VueCarteCreature
                          **/
                                 /** Si à la case 0 du tableau des composants dans le JPanel on trouve un objet de type VueCarteCreature **/ 
                                     VueCarteCreature carte = this.view.getCarteStockee();
                                     System.out.println("true : 2-1");

                                     /** on récupère la carte **/
                                     Creature crea = carte.getCarte();
                                     crea.setPosXDepart(getPosX());
                                     crea.setPosYDepart(getPosY());

                                     crea.setPosX(getPosX()+1);
                                     crea.afficherCarte();


                                     this.modele.getPlateau().getLigneCombatJoueur(this.modele.getJoueurActif()).ajouterCarte(this.getPosY(), crea);                        
                                     this.modele.getJoueurActif().setJligneCombat(this.modele.getPlateau().getLigneCombatJoueur(this.modele.getJoueurActif()));

                                     System.out.println("true : 2-2");


                                     this.modele.getJoueurActif().getJterrain().supprimerCarte(this.view.getCarteStockee().getCarte().getPosY());
                                     System.out.println("true : 2-3");
                                     this.view.supprimerCarteTerrain(this.view.getCarteStockee().getCarte().getPosY());
                                     this.modele.getPlateau().getLigneCombatJoueur(this.modele.getJoueurActif());

                                     this.modele.getPlateau().afficherPlateau();
                                  
                                     this.view.remplirLgneCombatJActifPanel(carte, getPosY());
                                     this.view.revalidate();
                                     this.view.repaint();
                                     this.carteStockee=null;
                                     this.view.setCarteStockee(null);
                                 }
                             
                    
                }
        }

        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    /**
     * @return the posY
     */
    public int getPosY() {
        return posY;
    }

    /**
     * @param posY the posY to set
     */
    public void setPosY(int posY) {
        this.posY = posY;
    }

    /**
     * @return the posX
     */
    public int getPosX() {
        return posX;
    }

    /**
     * @param posX the posX to set
     */
    public void setPosX(int posX) {
        this.posX = posX;
    }

    /**
     * @return the carteStockee
     */
    public VueCarteCreature getCarteStockee() {
        return carteStockee;
    }

    /**
     * @param carteStockee the carteStockee to set
     */
    public void setCarteStockee(VueCarteCreature carteStockee) {
        this.carteStockee = carteStockee;
    }
    


    
}
