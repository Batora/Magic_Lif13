
package magic.controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import magic.modele.Jeu;
import magic.vue.VueNewJeu;

/**
 *
 * @author Marc
 */
public class ControleurVueNewGame extends Controleur implements ActionListener{
    
    
    public ControleurVueNewGame(Jeu mod) {
        super(mod);
    }

    public void actionPerformed(ActionEvent e) {
        String src = ((JButton)e.getSource()).getText();
        System.out.println("src : " + src);
        if(src=="Jouer"){
            JFrame newJeu = new VueNewJeu(this);
        }
        else{
            System.exit(0);
        }
    }


    @Override
    public void mouseClicked(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
