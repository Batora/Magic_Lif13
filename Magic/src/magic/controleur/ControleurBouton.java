package magic.controleur;

import magic.modele.Jeu;
import magic.vue.Vue;
import magic.vue.VueJ1;
import magic.vue.VueJ2;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import magic.modele.Creature;
import magic.modele.Phase;

/**
 *
 * @author romain
 */
public class ControleurBouton extends Controleur{
   
    // Attributs
    
    Vue vue;
    
    // Constructeur
    
    public ControleurBouton (Jeu mod, Vue vu){
        super(mod);
        vue = vu;
    }

    
    
    
    
    // Methode
    @Override
    public void mouseClicked(MouseEvent me) {
        // Phase de défense ==> phase d'invocation
        if ( super.modele.getNumPhase()== 1)
        {           
            super.modele.nextPhase(); 
           
            super.modele.getJoueurActif().setRessource(super.modele.getNbTour()); 
           // ----- ICONE INVOCATION ----- //
           vue.getLabelIcone().setIcon(new ImageIcon("Images/Icon/iconInvocation.png"));
        }
        
        // Phase d'invocation ==> phase d'attaque
        else if ( super.modele.getNumPhase()== 2)
        {
            super.modele.nextPhase();
           
            // changer le label du bouton
            if(me.getSource() instanceof JButton){
                JButton nouvBouton = (JButton)me.getSource();
                nouvBouton.setText("Tour Suivant");
                vue.setJBouton(nouvBouton);
            }
            
            // ----- ICONE ATTAQUE ----- //
            vue.getLabelIcone().setIcon(new ImageIcon("Images/Icon/iconAttaque.png"));
           
        }
        // Phase d'Attaque ==> Phase de Défense autre Joueur
        else if ( super.modele.getNumPhase()== 3)
        {
            super.modele.nextPhase(); 
            super.modele.setNbTour(super.modele.getNbTour());
            VueJ2 vue2 = new VueJ2(super.modele);
            super.modele.getJ1().setRessource(0);
            super.modele.getJ2().setRessource(0);
            super.modele.getPlateau().getMainJoueur(super.modele.getJ2()).addObserver(vue2);
            super.modele.getPlateau().getTerrainJoueur(super.modele.getJ2()).addObserver(vue2);
            super.modele.getPlateau().getLigneCombatJoueur(super.modele.getJ2()).addObserver(vue2);
            super.modele.addObserver(vue2);
            super.modele.getJ2().addObserver(vue2);
            
            JOptionPane.showMessageDialog(null, "TOUR SUIVANT : " + super.modele.getNbTour()+ " \n C'est au tour de "+ vue.getNomJActif() +" de jouer.");
            
            vue.dispose();
            Phase pha=Phase.defense;
     
        
            super.modele.setPhaseActive(pha);
            super.modele.setNumPhase(0);
            
            // ----- ICONE DEFENSE ----- //
            vue2.getLabelIcone().setIcon(new ImageIcon("Images/Icon/iconDefense.png"));
            vue2.setVisible(true);
        }
        else 
        {
        System.out.println("Bouton presse vue2");
        
        }      
        
        
    }

    @Override
    public void mousePressed(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
