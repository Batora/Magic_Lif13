
package magic.controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import magic.modele.Carte;
import magic.modele.Creature;
import magic.modele.Jeu;
import magic.modele.Joueur;
import magic.modele.JoueurHumain;
import magic.modele.MainJoueur;
import magic.modele.Phase;
import magic.modele.Plateau;
import magic.vue.VueJ1;
import magic.vue.VueNewJeu;

/**
 *
 * @author Marc
 */
public class ControleurNouvellePartie extends Controleur implements ActionListener{

    protected String nj1,nj2;
    protected VueNewJeu viewNJ;
    
    public ControleurNouvellePartie(Jeu mod,VueNewJeu view){
        super(mod);
        this.viewNJ=view;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        System.out.println("clic new Jeu");
        
        nj1 = this.viewNJ.getNomJ1();
        nj2 = this.viewNJ.getNomJ2();
        
        magic.Magic.jeu.getJ1().setPseudo(nj1);
        magic.Magic.jeu.getJ2().setPseudo(nj2);
        viewNJ.dispose();
        viewNJ.setVisible(false);
        System.out.println("pseudo de J1 : " + magic.Magic.jeu.getJ1().getPseudo() + "\n");
        
        System.out.println("pseudo de J2 : " + magic.Magic.jeu.getJ2().getPseudo() + "\n");
        
        VueJ1 vueJ1 = new VueJ1(magic.Magic.jeu);
       
                
        magic.Magic.jeu.phaseDefense();
        
        String ress = Integer.toString(magic.Magic.jeu.getJoueurActif().getRessource());
        
        String ressMax = Integer.toString(magic.Magic.jeu.getJoueurActif().getRessourceMax());

        System.out.println("ress : " + ress );
       
        
        // On ajout les observeurs aux observables
        magic.Magic.jeu.getPlateau().getMainJoueur(magic.Magic.jeu.getJ1()).addObserver(vueJ1);
        magic.Magic.jeu.getPlateau().getTerrainJoueur(magic.Magic.jeu.getJ1()).addObserver(vueJ1);
        magic.Magic.jeu.getPlateau().getLigneCombatJoueur(magic.Magic.jeu.getJ1()).addObserver(vueJ1);
        magic.Magic.jeu.addObserver(vueJ1);
        magic.Magic.jeu.getJ1().addObserver(vueJ1);
        
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
