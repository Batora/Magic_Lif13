
package magic.controleur;
import java.awt.event.MouseListener;
import magic.modele.Jeu;


/**
 *
 * @author P1302812 & P1509371
 */
public abstract class Controleur implements MouseListener {


    /* On initialise le modele dans le controleur */
    protected Jeu modele;
    
    //Constructeur
   
    public Controleur (Jeu mod){
        this.modele = mod;
    }
    
    public Controleur (){
    }
    
    
}
