
package magic.controleur;

import java.awt.event.MouseEvent;
import magic.modele.Carte;
import magic.modele.Creature;
import magic.modele.Jeu;
import java.awt.event.MouseEvent;

/**
 *
 * @author Marc
 */
public class ControleurJeu extends Controleur{

    // Attributs
    int i;
    int j;
    private Carte carteTemp;
    int deplacement = 0;
    
    
    
    //Controleurs
    public ControleurJeu(Jeu mod, int i, int j, Carte carte) {
        super(mod);
        this.i = i;
        this.j = j;
        this.carteTemp = carte;
    }
    
    
    //Methode

    @Override
    public void mouseClicked(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //System.out.println("position : x " + i + " et y :" + j);
        System.out.println("On passe par le controleur du jeu");
        if (i == 0){
            // Main joueur
                  
            /* -- Phase d'invocation -- */
            if (carteTemp instanceof Creature){
                Creature carteTemporaire = (Creature)carteTemp;
                if (super.modele.getNumPhase()==2 && (carteTemporaire.getCout() <= super.modele.getJoueurActif().getRessource())) 
                {
                    if (super.modele.getPlateau().getTerrainJoueur(super.modele.getJoueurActif()).ajouterCarte(j, carteTemp))
                    {
                        super.modele.getPlateau().getMainJoueur(super.modele.getJoueurActif()).supprimerCarte(j);
                        super.modele.getJoueurActif().getJmain().supprimerCarte(j);

                    
                        super.modele.getJoueurActif().setRessource(super.modele.getJoueurActif().getRessource() - carteTemporaire.getCout());
                    }
                }
                deplacement = 1;
                
            }
        }
        
        
        if (i == 1){
            // Terrain joueur
            
            /* -- Phase de defense -- */
            if (super.modele.getNumPhase() == 1)
            {
                //parcours ligne combat du joueur adverse
                for(int i=0;i<super.modele.getPlateau().getLigneCombatJoueur(super.modele.getJoueurAdverse()).getCartes().length;i++){
                    //si sa ligne de combat contient des cartes
                    if (super.modele.getPlateau().getLigneCombatJoueur(super.modele.getJoueurAdverse()).getCartes()[i] != null)
                    {
                        //parcours des cartes du joueur actif
                        for(int j=0;j<super.modele.getPlateau().getTerrainJoueur(super.modele.getJoueurActif()).getCartes().length;j++){
                            //si on a cliqué sur une carte du terrain joueur actif
                            Creature carteCreaTemp = (Creature) super.modele.getPlateau().getTerrainJoueur(super.modele.getJoueurActif()).getCartes()[j];
                               
                            //on s'assure que la carte n'est pas fatiguée
                            if (carteCreaTemp.isEstFatigue()==false)
                            {
                                 // on s'assure qu'on peut placer une carte à cet emplacement
                                if(super.modele.getPlateau().getLigneCombatJoueur(super.modele.getJoueurActif()).getCarte(i)==null){
                                    super.modele.getPlateau().getLigneCombatJoueur(super.modele.getJoueurActif()).ajouterCarte(i,carteCreaTemp);
                                    super.modele.getPlateau().getTerrainJoueur(super.modele.getJoueurActif()).supprimerCarte(j);
                                }
                                else if(super.modele.getPlateau().getLigneCombatJoueur(super.modele.getJoueurActif()).getCarte(i+1)==null && i+1<super.modele.getPlateau().getTerrainJoueur(super.modele.getJoueurActif()).getCartes().length){
                                    super.modele.getPlateau().getLigneCombatJoueur(super.modele.getJoueurActif()).ajouterCarte(i+1,carteCreaTemp);
                                    super.modele.getPlateau().getTerrainJoueur(super.modele.getJoueurActif()).supprimerCarte(j);
                                }
                            }
                            
                        }
                    }
                }           
            }  
            
            /* -- Phase d'attaque -- */
            
            if (super.modele.getNumPhase()==3) 
            {
                Creature carteTerrain = (Creature) carteTemp;
                if (carteTerrain.isEstFatigue()== false)
                {
                    super.modele.getPlateau().getTerrainJoueur(super.modele.getJoueurActif()).initCarteAtk(j);

                }                               
            }

        }
        
        if ( i == 2){
            // Ligne Combat Joueur
            
             /* -- Deplacement carte Phase de defense -- */
            if (super.modele.getNumPhase() == 1)
            {
                if (super.modele.getPlateau().getTerrainJoueur(super.modele.getJoueurActif()).getDeplacement()>= 0)
                {
                    super.modele.getPlateau().getTerrainJoueur(super.modele.getJoueurActif()).initCarteDef(super.modele.getPlateau().getTerrainJoueur(super.modele.getJoueurActif()).getDeplacement(), j, super.modele.getPlateau().getLigneCombatJoueur(super.modele.getJoueurActif()));
                    super.modele.getPlateau().getTerrainJoueur(super.modele.getJoueurActif()).setPosCarteDeplacement(-1);
                }
            }
            

            if ( super.modele.getNumPhase()==3 || super.modele.getNumPhase()==1)
            {
                //System.out.println("La ligne de combat : " + super.modele.getPlateau().getTerrainJoueur(super.modele.getJoueurCourant()).getCarteToString());
                
            }
            
        }
    
    
    }

    @Override
    public void mousePressed(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}


