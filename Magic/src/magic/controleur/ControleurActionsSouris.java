/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package magic.controleur;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.MouseInputAdapter;
import magic.modele.Creature;
import magic.modele.Jeu;
import magic.vue.VueCarteCreature;
import magic.vue.VueJ1;

/**
 *
 * @author Marc
 */
public class ControleurActionsSouris implements MouseListener{
    
    protected int posY, posX;
    protected VueJ1 view;
    protected Jeu modele;

    public ControleurActionsSouris(Jeu modele, VueJ1 vue, int i, int j) {
        this.posX=i;
        this.posY=j;
        this.view = vue;
        this.modele=modele;
    }
    

    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("clic ! sur ");
        System.out.println("position :  " + this.posX + " y : " + this.posY);
        
        if(this.posX<1){
            /** si l'évenement capté est dans la main du joueur actif **/
           if(e.getSource() instanceof JPanel){
               JPanel panelClique=(JPanel)e.getSource();

                System.out.println("true : 1");
                /** on s'assure que la carte sur laquelle on clique est bien de type 
                 * VueCarteCreature
                 **/
                    if(panelClique.getComponents()[0] instanceof VueCarteCreature){
                        VueCarteCreature carte = (VueCarteCreature)panelClique.getComponents()[0];
                        System.out.println("true : 2-1");
                        
                        /** on récupère la carte **/
                        Creature crea = carte.getCarte();
                        crea.setPosXDepart(posX);
                        crea.setPosYDepart(posY);
                        
                        crea.setPosX(posX+1);
                        crea.afficherCarte();
                        
                        
                        this.modele.getPlateau().getTerrainJoueur(this.modele.getJoueurActif()).ajouterCarte(this.posY, crea);                        
                        this.modele.getJoueurActif().setJterrain(this.modele.getPlateau().getTerrainJoueur(this.modele.getJoueurActif()));
                        
                        System.out.println("true : 2-2");
                        
                        this.view.remplirTerrainJActif(this.modele.getJoueurActif().getJterrain(), posX, posY);
                        this.modele.getJoueurActif().getJmain().supprimerCarte(posY);
                        System.out.println("true : 2-3");
                        
                        this.modele.getPlateau().getTerrainJoueur(this.modele.getJoueurActif()).updateTerrain();
                        this.modele.getPlateau().afficherPlateau();
                        
                        this.view.repaint();
                    }
                System.out.println("true : 3");
           }
          
        }
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }
    


    
}
