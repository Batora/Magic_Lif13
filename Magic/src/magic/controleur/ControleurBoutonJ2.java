package magic.controleur;

import java.awt.Button;
import java.awt.Component;
import magic.modele.Jeu;
import magic.vue.Vue;
import magic.vue.VueJ1;
import magic.vue.VueJ2;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import magic.modele.Phase;

/**
 *
 * @author Marc
 */
public class ControleurBoutonJ2 extends Controleur{
   
    // Attributs
    
    VueJ2 vue2;
    
    // Constructeur
    
    public ControleurBoutonJ2 (Jeu mod, VueJ2 vu){
        super(mod);
        vue2 = vu;
    }

    
      @Override
    public void mouseClicked(MouseEvent me) {
        // Phase de défense ==> phase d'invocation
        if ( super.modele.getNumPhase()== 1)
        {
            super.modele.nextPhase();       
            
            super.modele.getJoueurActif().setRessource(super.modele.getNbTour()); 
            
           // ----- ICONE INVOCATION ----- //
            vue2.getLabelIcone().setIcon(new ImageIcon("Images/Icon/iconInvocation.png"));
        }
        
        // Phase d'invocation ==> phase d'attaque
        else if ( super.modele.getNumPhase()== 2)
        {
            super.modele.nextPhase();
            
            
            // changer le label du bouton
            if(me.getSource() instanceof JButton){
                JButton nouvBouton = (JButton)me.getSource();
                nouvBouton.setText("Tour Suivant");
                vue2.setJBouton(nouvBouton);
            }
            // ----- ICONE ATTAQUE ----- //
            vue2.getLabelIcone().setIcon(new ImageIcon("Images/Icon/iconAttaque.png"));
           
        }
        // Phase d'Attaque ==> Phase de Défense autre Joueur
        else if ( super.modele.getNumPhase()== 3)
        {
            super.modele.nextPhase();
            super.modele.setNbTour(super.modele.getNbTour()+1);
            VueJ1 vue = new VueJ1(super.modele);
            super.modele.getJ1().setRessource(0);
            super.modele.getJ2().setRessource(0);
            super.modele.getPlateau().getMainJoueur(super.modele.getJ1()).addObserver(vue);
            super.modele.getPlateau().getTerrainJoueur(super.modele.getJ1()).addObserver(vue);
            super.modele.getPlateau().getLigneCombatJoueur(super.modele.getJ1()).addObserver(vue);
            super.modele.addObserver(vue);
            super.modele.getJ1().addObserver(vue);
            
            JOptionPane.showMessageDialog(null, "TOUR SUIVANT : " + super.modele.getNbTour() + " \n C'est au tour de "+ vue.getNomJActif() +" de jouer.");
            
              // On affiche le plateau -- MODE CONSOLE --
            super.modele.getPlateau().afficherPlateau();
           
            
            vue2.dispose();
            
            // ----- ICONE DEFENSE ----- //
            vue.getLabelIcone().setIcon(new ImageIcon("Images/Icon/iconDefense.png"));
            
            
            vue.setVisible(true);
        }
        else 
        {
        System.out.println("Bouton presse vue");
        
        }      

        
    }

    @Override
    public void mousePressed(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
