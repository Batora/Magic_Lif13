
package magic.vue;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Observable;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import magic.controleur.Controleur;
import magic.controleur.ControleurNouvellePartie;
import magic.modele.Jeu;


/**
 * Fenêtre apparaissant lors de la génération d'une nouvelle partie
 * @author Marc
 */
public class VueNewJeu  extends JFrame{

    protected Controleur controleur;
    protected JLabel nomJ1, nomJ2;
    protected JButton butConfimer , butAnnuler;
    protected JTextField nJ1,nJ2;
    protected String nameJ1,nameJ2;
    protected Jeu jeu;
    
    public VueNewJeu(Controleur ctrl){
        super("Magic - Nouvelle Partie");
        
        jeu = magic.Magic.jeu;
      
      controleur=ctrl;

      WindowListener l = new WindowAdapter() {
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      };

      addWindowListener(l);
      setSize(400,150);
      this.setLocationRelativeTo(null);
      // icone cartes 
      this.setIconImage(new ImageIcon("Images/Icon/icon.png").getImage());
      this.initComponent();
      System.out.println("nom j1 :" + nameJ1 + "et nom j2 vue : " + nameJ2);
     
   }

    private void initComponent(){
      
        
      JPanel champNJ1 = new JPanel();
      nomJ1 = new JLabel("Nom du Joueur 1 : ");
      nJ1= new JTextField();
      
      Box b1=Box.createHorizontalBox();
      b1.add(champNJ1.add(nomJ1));
      
      Box b2=Box.createHorizontalBox();
      b2.add(champNJ1.add(nJ1));
      
      Box b3=Box.createVerticalBox();
      b3.add(b1);
      b3.add(b2);
      
      champNJ1.add(b3);
      
      JPanel champNJ2 = new JPanel();
      nomJ2 = new JLabel("Nom du Joueur 2 : ");
      nJ2= new JTextField();
      
      Box b4=Box.createHorizontalBox();
      b4.add(champNJ2.add(nomJ2));
      
      Box b5=Box.createHorizontalBox();
      b5.add(champNJ2.add(nJ2));
      
      Box b6=Box.createVerticalBox();
      b6.add(b4);
      b6.add(b5);
      
      champNJ2.add(b6);
      
      JPanel buttons = new JPanel();
      butConfimer = new JButton("Lancer la partie");
      nameJ1 = nJ1.getText();
      nameJ2 = nJ2.getText();
      butConfimer.addActionListener(new ControleurNouvellePartie(jeu,this));
      
      butAnnuler = new JButton("Annuler");
      
      buttons.add(butConfimer);
      buttons.add(butAnnuler);
      
      JPanel container = new JPanel();
      container.add(champNJ1);
      container.add(champNJ2);
      container.add(buttons);
      
      this.add(container, BorderLayout.CENTER);
      setVisible(true);
    
    
    }
    
    public String getNomJ1(){
        return this.nJ1.getText();
    }
    
    public String getNomJ2(){
        return this.nJ2.getText();
    }
   
    
}
