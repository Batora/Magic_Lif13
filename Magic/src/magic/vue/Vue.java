
package magic.vue;

import java.util.Observer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Observable;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.Border;
import magic.controleur.ControleurBouton;
import magic.modele.Jeu;

/**
 *
 * @author Marc
 */

public abstract class Vue extends JFrame implements Observer{
    
// ATTRIBUTS //
   protected JPanel container, pannelGlobal, panGlobalJActif, panGlobalJAdverse, panMainJActif, panMainJAdverse, panTerrainJActif, panTerrainJAdverse,panLCJActif, panLCAdverse, panButtonNextPhase;
   protected JButton bNextPhase;
   protected JTable tabJActif;
   protected JTable tabJAdverse;
   protected JPanel[][] VuePlateauJActif;
   protected JPanel[][] VuePlateauJAdverse;
   protected Container containerPlateau;
   protected int tailleLigne , tailleColonne;
   protected Jeu model;
   protected VueCarteCreature carteStockee;

   
   protected JLabel labelJAdverse;
   protected JLabel labelJActif;
   protected JLabel ressourcesJActif;
   protected JLabel ressourcesJAdverse;
   protected JLabel imageIcon;
   protected JLabel nbTour;
   protected JLabel numPhase;
   protected JLabel nomPhase;
   
  
   
   // CONSTRUCTEUR //
   public Vue (String titre, Jeu mod){
       this.model=mod;
       this.setTitle(titre);
       this.setSize(1000, 700);
       this.setLocationRelativeTo(null);
       this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       WindowListener l = new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                System.exit(0);
            }
        };
       
        this.setIconImage(new ImageIcon("Images/Icon/icon.png").getImage());
       
        labelJAdverse = new JLabel();
        labelJActif = new JLabel();
        ressourcesJActif = new JLabel();
        ressourcesJAdverse = new JLabel();
        imageIcon = new JLabel();
        nbTour = new JLabel();
        numPhase = new JLabel();
        nomPhase= new JLabel();
        
        panGlobalJActif = new JPanel();
        panGlobalJAdverse = new JPanel();
      
        nbTour.setText("Tour : " + mod.getNbTour()+ "  ");
        numPhase.setText("Phase : " + mod.getNumPhase()+ " ");
        nomPhase.setText(mod.getPhaseActive().toString());
        ressourcesJActif.setText("Ressources : 0 / "+ mod.getNbTour());
        ressourcesJAdverse.setText("Ressources : 0 / "+mod.getNbTour());
        
          
        /* --- PANEL JOUEUR ACTIF --------- */
        labelJActif.setText(mod.getJoueurActif().getPseudo()+ "  -  " + " Points de vie : " + mod.getJoueurActif().getPDV()+"/"+mod.getJoueurAdverse().getPDVMax());
        labelJActif.setPreferredSize(new Dimension(700,50));
        labelJActif.setHorizontalAlignment(JLabel.CENTER);
        panGlobalJActif.setLayout(new FlowLayout());
        panGlobalJActif.add(labelJActif);
        
        
        /* --- PANEL JOUEUR ADVERSE ------- */
        labelJAdverse.setText(mod.getJoueurAdverse().getPseudo()+ "  -  " + " Points de vie : " + mod.getJoueurAdverse().getPDV()+"/"+mod.getJoueurAdverse().getPDVMax());
        labelJAdverse.setPreferredSize(new Dimension(700,50));
        labelJAdverse.setHorizontalAlignment(JLabel.CENTER);
        panGlobalJAdverse.setLayout(new FlowLayout());
        panGlobalJAdverse.add(labelJAdverse);
                
       
        
      /* ~~~~~~~~~~~~~~~~~~~~ PANEL GLOBAL JOUEUR  ~~~~~~~~~~~~~~~~~~~~~~~~~~  */
     

 //       panGlobalJAdverse.setLayout(new FlowLayout());
        panGlobalJAdverse.add(ressourcesJAdverse);
        panGlobalJActif.add(ressourcesJActif);
        
      /* ~~~~~~~~~~~~~~~~~~~~~ PANEL CREATION PLATEAU ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  */
       
        // ~ Modifier les valeurs de ligne et de colonne pour afficher un plus grand plateau
        tailleLigne = 3;
        tailleColonne=4; // passer la taille du plateau de jeu grâce au contrôleur
              
        VuePlateauJActif=new JPanel[tailleLigne][tailleColonne];
        VuePlateauJAdverse=new JPanel[tailleLigne][tailleColonne];
       // VuePlateau=new GridLayout(tailleLigne, tailleColonne);
        Border blackline = BorderFactory.createLineBorder(Color.black);
      
      /* ~~~~~~~~~~~~~~~~~~~~ PANEL CHANGEMENT DE PHASE  ~~~~~~~~~~~~~~~~~~~~~~~~~~  */
      
      // -- utilisation des box layouts pour arranger facilement le layout --
      
      
        // ----- NUMERO DU TOUR ------
        panButtonNextPhase = new JPanel();
        Box b1=Box.createHorizontalBox();
        b1.add(panButtonNextPhase.add(nbTour));
        Box b2=Box.createHorizontalBox();
        
        // ----- NUMERO DE PHASE ------
        b2.add(panButtonNextPhase.add(numPhase));
        Box b3=Box.createHorizontalBox();
        
        // ----- NOM DE PHASE ------
        b3.add(panButtonNextPhase.add(nomPhase));
        Box b4=Box.createHorizontalBox();
        
        // ----- BOUTON DE PROCHAINE PHASE ------
        bNextPhase=new JButton("Phase Suivante");
        b4.add(panButtonNextPhase.add(bNextPhase));
        
        
        // ----- IMAGE DE PHASE ------
        JPanel image = new JPanel();
        imageIcon = new JLabel(new ImageIcon("Images/Icon/iconDefense.png"));
        image.setLayout(new BorderLayout());
        image.add(imageIcon, BorderLayout.CENTER);
        image.setBorder(BorderFactory.createLineBorder(Color.black));
        
        
        
        Box b5=Box.createVerticalBox();
        b5.add(b1);
        b5.add(b2);
        b5.add(imageIcon);
        b5.add(b3);
        b5.add(b4);
        
        panButtonNextPhase.add(b5);
        JPanel panButtonGlobal=new JPanel();
        panButtonGlobal.setLayout(new BorderLayout());
        panButtonGlobal.add(panButtonNextPhase, BorderLayout.SOUTH);
        
        
   
      /* ~~~~~~~~~~~~~~~~~~~ AJOUTS PANELS AU CONTAINER ~~~~~~~~~~~~~~~~~~~~~~~~  */
        container = new JPanel();
        JPanel containerGeneral = new JPanel();
        
        container.setLayout(new BorderLayout());
        container.add(panGlobalJAdverse,BorderLayout.NORTH);
        containerPlateau = new JPanel();
        containerPlateau.setLayout(new GridLayout(tailleLigne*2,tailleColonne));
        // *2 car il va contenir le joueur actif et adverse
        
     
        container.add(panButtonGlobal,BorderLayout.EAST);
        container.add(panGlobalJActif,BorderLayout.SOUTH);
        container.add(containerPlateau);
        
        containerGeneral.add(container,BorderLayout.CENTER);
        
        this.add(containerGeneral);
      
        /* ---- AJOUT AU CONTAINER PRINCIPAL --- */
        
        pannelGlobal = new JPanel();
        pannelGlobal.setLayout(new BorderLayout());
        //panelGeneral.setBackground(Color.red);
        pannelGlobal.add(panGlobalJActif, BorderLayout.SOUTH);
        pannelGlobal.add(panGlobalJAdverse, BorderLayout.NORTH);
        pannelGlobal.add(panButtonNextPhase, BorderLayout.EAST);
        pannelGlobal.add(containerPlateau, BorderLayout.CENTER);
        
        JPanel container = new JPanel();
        container.setLayout(new BorderLayout());
        container.setBackground(Color.blue);
        container.add(pannelGlobal, BorderLayout.CENTER);
        
        this.setContentPane(container);
        
        this.setVisible(true);
    }
    
    /**
     * @return the nomJActif
     */
    public String getNomJActif() {
        return this.model.getJoueurActif().getPseudo();
    }

    /**
     * @return the nomJAdverse
     */
    public String getNomJAdverse() {
        return this.model.getJoueurAdverse().getPseudo();
    }

    /**
     * @return the ressourcesJActif
     */
    public JLabel getRessourcesJActif() {
        return ressourcesJActif;
    }

    /**
     * @return the ressourcesJAdverse
     */
    public JLabel getRessourcesJAdverse() {
        return ressourcesJAdverse;
    }
    
    public JButton getBouton(){
        return bNextPhase;
    }
    
    public void setJBouton(JButton nouveauBouton){
        this.bNextPhase=nouveauBouton;
    }

    /**
     * @return the nbTour
     */
    public JLabel getNbTour() {
        return nbTour;
    }

    /**
     * @return the numPhase
     */
    public JLabel getNumPhase() {
        return numPhase;
    }

    /**
     * @return the nomPhase
     */
    public JLabel getNomPhase() {
        return nomPhase;
    }
    
   
    public JLabel getLabelJActif() {
        return labelJActif;
    }
    
    public JLabel getLabelJAdverse() {
        return labelJAdverse;
    }
    
    public JLabel getLabelIcone() {
        return imageIcon;
    }
    
    @Override
    public void update(Observable o, Object o1) {
    }
    
}
