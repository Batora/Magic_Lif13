
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package magic.vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import magic.controleur.ControleurBoutonJ2;
import magic.controleur.ControleurJeu;
import magic.modele.Creature;
import magic.modele.Jeu;

/**
 * Interface graphique en Swing du joueur 2
 * @author P1302812 & P1509371
 */
public class VueJ2 extends Vue implements MouseListener{
    
    // Attributs
    VueCarteCreature[] carteCreatureJ1;
    VueCarteCreature[] carteCreatureJ2;

    protected JPanel[][] vuePlatJActif = new JPanel[3][4];
    protected JPanel[][] vuePlatJAdverse = new JPanel[3][4];
    
    
    
    // Constructeur
    
    public VueJ2(Jeu modele)
    {
        super ("Magic - Jeu de cartes", modele);
        super.nbTour.setText("Tour : " + super.model.getNbTour()+ "  ");

        
        remplirPlateauJAdverse(super.containerPlateau);
        remplirPlateauJActif(super.containerPlateau);
        
        remplirMainJ2();
        remplirMainJ1();
        
        remplirTerrainJ2();
        remplirTerrainJ1();
        
        remplirLigneCombatJ2();
        remplirLigneCombatJ1();
        
        
        super.containerPlateau.revalidate();
        super.containerPlateau.repaint();
        
        // Ajout des listeners sur le bouton de droite
        
        super.bNextPhase.addMouseListener(new ControleurBoutonJ2(modele, this));
    }
    
    

    
    // Methode
    
    public void remplirPlateauJActif(Container platJeu){
        for (int i = 2; i > -1; i --){
            for (int j = 0; j < 4; j++){
               if(i==0){
                    vuePlatJActif[i][j]= new JPanel();
                    vuePlatJActif[i][j].setBackground(Color.decode("#f7f0dc"));
                    vuePlatJActif[i][j].setBorder(BorderFactory.createLineBorder(Color.black));
                    platJeu.add(vuePlatJActif[i][j]);
                }
                
                if(i==1){
                    vuePlatJActif[i][j] = new JPanel();
                    vuePlatJActif[i][j].setBackground(Color.decode("#66cdaa"));
                    vuePlatJActif[i][j].setBorder(BorderFactory.createLineBorder(Color.black));
                    platJeu.add(vuePlatJActif[i][j]);
                }
                if(i==2){
                    vuePlatJActif[i][j] = new JPanel();
                    vuePlatJActif[i][j].setBackground(Color.decode("#f7dce4"));
                    vuePlatJActif[i][j].setBorder(BorderFactory.createLineBorder(Color.black));
                    platJeu.add(vuePlatJActif[i][j]);
                    
                }
            }
        }
    }
    
    public void remplirPlateauJAdverse(Container platJeu){
        for (int i = 0; i <3; i ++){
            for (int j = 0; j < 4; j++){
                if(i==2){
                    vuePlatJAdverse[i][j]= new JPanel();
                    vuePlatJAdverse[i][j].setBackground(Color.decode("#f7dce4"));
                    vuePlatJAdverse[i][j].setBorder(BorderFactory.createLineBorder(Color.black));
                    platJeu.add(vuePlatJAdverse[i][j]);
                }
                
                if(i==1){
                    vuePlatJAdverse[i][j] = new JPanel();
                    vuePlatJAdverse[i][j].setBackground(Color.decode("#66cdaa"));
                    vuePlatJAdverse[i][j].setBorder(BorderFactory.createLineBorder(Color.black));
                    platJeu.add(vuePlatJAdverse[i][j]);
                }
                if(i==0){
                    vuePlatJAdverse[i][j] = new JPanel();
                    vuePlatJAdverse[i][j].setBackground(Color.decode("#f7f0dc"));
                    vuePlatJAdverse[i][j].setBorder(BorderFactory.createLineBorder(Color.black));
                    platJeu.add(vuePlatJAdverse[i][j]);
                    
                }
            }
        }
    }
    
    
    public void remplirMainJ2 (){
        for (int i = 0 ; i < 1 ; i++){
            for (int j = 0 ; j< 4; j++){
                //System.out.println("Au debut : " + super.modele.getPlateau().getMainJoueur(this.modele.getJoueurCourant()).getCarte(j));
                Creature carteCrea = (Creature) super.model.getJ2().getJmain().getCarte(j);
                if (carteCrea != null)
                {
                   
                    VueCarteCreature vueCarteCrea = new VueCarteCreature(carteCrea);
                    vuePlatJActif[i][j].setLayout(new BorderLayout());
                    vuePlatJActif[i][j].addMouseListener(new ControleurJeu(super.model, i , j, carteCrea));
                    vuePlatJActif[i][j].add(vueCarteCrea, BorderLayout.CENTER);
                }
                else 
                {
                   
                    vuePlatJActif[i][j].removeAll();
                }
                
                
            }
        }
      
    }
    
    public void remplirMainJ1 (){
        for (int i = 0 ; i < 1; i++){
            for (int j = 0 ; j< 4; j++){
                Creature carteCrea = (Creature) super.model.getJ1().getJmain().getCarte(j);
                
                if (carteCrea != null)
                {
                    VueCarteCreature carteCreaVue = new VueCarteCreature(carteCrea);
                    vuePlatJAdverse[i][j].setLayout(new BorderLayout());
                    vuePlatJAdverse[i][j].add(carteCreaVue, BorderLayout.CENTER);
                    
                }
                else 
                {
                    vuePlatJAdverse[i][j].removeAll();
                    
                }
            }
        }
    }
     public void remplirTerrainJ2 (){
        for (int i = 1 ; i < 2 ; i++){
            for (int j = 0 ; j< 4; j++){
                Creature carteCrea = (Creature) super.model.getPlateau().getTerrainJoueur(this.model.getJoueurActif()).getCarte(j);
                if (carteCrea != null)
                {
                    VueCarteCreature vueCarteCrea = new VueCarteCreature(carteCrea);
                    vuePlatJActif[i][j].setLayout(new BorderLayout());
                    vuePlatJActif[i][j].addMouseListener(new ControleurJeu(super.model, i , j, carteCrea));
                    vuePlatJActif[i][j].add(vueCarteCrea, BorderLayout.CENTER);
                    
                    
                }
                else 
                {
                    vuePlatJActif[i][j].removeAll();
                    
                }
                
                
            }
        }
      
    }
    
    public void remplirTerrainJ1 (){
        for (int i = 1 ; i < 2; i++){
            for (int j = 0 ; j< 4; j++){
                Creature carteCrea = (Creature) super.model.getPlateau().getTerrainJoueur(this.model.getJoueurAdverse()).getCarte(j);
                if (carteCrea != null)
                {
                    VueCarteCreature carteCreaVue = new VueCarteCreature(carteCrea);
                    vuePlatJAdverse[i][j].setLayout(new BorderLayout());
                    vuePlatJAdverse[i][j].add(carteCreaVue, BorderLayout.CENTER);                    
                }
                else 
                {
                    vuePlatJAdverse[i][j].removeAll();
                }
            }
        }
    }
    
    public void remplirLigneCombatJ2 (){
        for (int i = 2 ; i < 3 ; i++){
            for (int j = 0 ; j< 4; j++){
                Creature carteCrea = (Creature) super.model.getPlateau().getLigneCombatJoueur(this.model.getJoueurActif()).getCarte(j);
                if (carteCrea != null)
                {
                    VueCarteCreature vueCarteCrea = new VueCarteCreature(carteCrea);
                    vuePlatJActif[i][j].setLayout(new BorderLayout());
                    vuePlatJActif[i][j].addMouseListener(new ControleurJeu(super.model, i , j, carteCrea));
                    vuePlatJActif[i][j].add(vueCarteCrea, BorderLayout.CENTER);
                    
                    
                }
                else 
                {
                    vuePlatJActif[i][j].addMouseListener(new ControleurJeu(super.model, i , j, carteCrea));
                    vuePlatJActif[i][j].removeAll();
                    
                }
                
                
            }
        }
      
    }
    
    public void remplirLigneCombatJ1 (){
        for (int i = 2 ; i < 3; i++){
            for (int j = 0 ; j< 4; j++){
                Creature carteCrea = (Creature) super.model.getPlateau().getLigneCombatJoueur(this.model.getJoueurAdverse()).getCarte(j);
                if (carteCrea != null)
                {
                    VueCarteCreature carteCreaVue = new VueCarteCreature(carteCrea);
                    vuePlatJAdverse[i][j].setLayout(new BorderLayout());
                    vuePlatJAdverse[i][j].add(carteCreaVue, BorderLayout.CENTER);
                    
                }
                else 
                {
                    vuePlatJAdverse[i][j].removeAll();
                }
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    

    @Override
    public void update(Observable o, Object o1) {
        
            remplirMainJ2();
            remplirMainJ1();
            remplirTerrainJ2();  
            remplirTerrainJ1();
            remplirLigneCombatJ2();
            remplirLigneCombatJ1();
            super.labelJActif.setText(super.model.getJoueurActif().getPseudo()+ "  -  " + " Points de vie : " + super.model.getJoueurActif().getPDV() + "/" + super.model.getJoueurActif().getPDVMax());
            super.labelJAdverse.setText(super.model.getJoueurAdverse().getPseudo()+ "  -  " + " Points de vie : " + super.model.getJoueurAdverse().getPDV() + "/" + super.model.getJoueurAdverse().getPDVMax());
            super.numPhase.setText("Phase : " + super.model.getNumPhase());
            super.ressourcesJActif.setText("Ressources : " +super.model.getJoueurActif().getRessource() + " / " + super.model.getNbTour());
            super.nomPhase.setText(model.getPhaseActive().toString());

            super.containerPlateau.revalidate();
            super.containerPlateau.repaint();
            
        
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}