
package magic.vue;

import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import magic.controleur.Controleur;
import magic.modele.Jeu;


public abstract class VuePlateauJeu  extends JFrame{

  
  
   
   //instance de notre controleur 
   protected Controleur controller;
    private Jeu model;
    
    public VuePlateauJeu(Controleur ctrl){
        super("Magic - Jeu de cartes");
        this.controller=ctrl;

        
        
        JMenuBar jm = new JMenuBar();
        //JMenu jm = new JMenu();
        JMenu m = new JMenu("Jeu");
        
        JMenuItem mi = new JMenuItem("Nouvelle Partie");
        JMenuItem mi2 = new JMenuItem("Quitter Partie");
        //ItemListener i = new Item
        
        m.add(mi);
        m.add(mi2);
        
        jm.add(m);
        
        this.setJMenuBar(jm);
        //addWindowListener(l);
        setSize(1000,1000);
        this.setLocationRelativeTo(null);
        this.setBackground(Color.decode("#f0f1ee"));
        // icone cartes 
        this.setIconImage(new ImageIcon("Images/Icon/icon.png").getImage());
        
        

        
     
    }
    
    
}
