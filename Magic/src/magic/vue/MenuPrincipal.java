
package magic.vue;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import magic.controleur.Controleur;
import magic.controleur.ControleurVueNewGame;
import magic.modele.Jeu;

/**
 * Menu principal de l'application, permet de sélectionner la difficulté,
 * définir les noms des joueurs 1 et 2
 * @author P1302812 & P1509371
 */
public class MenuPrincipal extends JFrame{

   private JLabel imageBG, txtPlay, txtQuit;
   private JPanel container, panImg, panButtons, panButtonPlay, panButtonQuit;
   private JButton jouer, quitter;
   private Controleur controleur;
   
   protected Jeu jeu;
   
    public MenuPrincipal(Controleur ctrl) {
      super("Magic - Jeu de cartes");
      
      jeu = magic.Magic.jeu;
      controleur=ctrl;

      WindowListener l = new WindowAdapter() {
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      };

      addWindowListener(l);
      setSize(800,475);
      this.setLocationRelativeTo(null);
      // icone cartes 
      this.setIconImage(new ImageIcon("Images/Icon/icon.png").getImage());
      this.initComponent();
     
   }

    private void initComponent(){
        
      /* ~~~~~~~~~~~~~~~~~~~~ PANEL IMAGE CARTE MAGIC ~~~~~~~~~~~~~~~~~~~~~~~~~~  */
      // image carte
      imageBG = new JLabel(new ImageIcon("Images/background/Magic.jpg"));
      panImg = new JPanel();
      panImg.setLayout(new BorderLayout());
      panImg.add(imageBG);
      
      /* ~~~~~~~~~~~~~~~~~~~~        PANEL BOUTONS     ~~~~~~~~~~~~~~~~~~~~~~~~~~  */
      txtPlay = new JLabel("Jouer au jeu : ");
      
      jouer = new JButton("Jouer");
      jouer.setName("play");
      jouer.addActionListener(new ControleurVueNewGame(jeu));
      
      txtQuit = new JLabel("Quitter le jeu : ");
      quitter = new JButton("Quitter");
      quitter.setName("quit");
      quitter.addActionListener(new ControleurVueNewGame(jeu));
      panButtonPlay = new JPanel();
      
      
      Box b1=Box.createHorizontalBox();
      b1.add(panButtonPlay.add(txtPlay));
      Box b2=Box.createHorizontalBox();
      b2.add(panButtonPlay.add(jouer));
      Box b3=Box.createVerticalBox();
      b3.add(b1);
      b3.add(b2);
     
      panButtonPlay.add(b3);
      
      panButtonQuit= new JPanel();
      
      Box b4=Box.createHorizontalBox();
      b4.add(panButtonQuit.add(txtQuit));
      Box b5=Box.createHorizontalBox();
      b5.add(panButtonQuit.add(quitter));
      Box b6=Box.createVerticalBox();
      b6.add(b4);
      b6.add(b5);
      
      panButtonQuit.add(b6);
      
      panButtons=new JPanel();
     
      Box b7=Box.createHorizontalBox();
      b7.add(panButtons.add(panButtonPlay));
      Box b8=Box.createHorizontalBox();
      b8.add(panButtons.add(panButtonQuit));
      
      Box b9=Box.createVerticalBox();
      b9.add(b7);
      b9.add(b8);
      panButtons.add(b9);
      
      
      /* ~~~~~~~~~~~~~~~~~~~ AJOUTS PANELS AU CONTAINER ~~~~~~~~~~~~~~~~~~~~~~~~  */
      container = new JPanel();
      container.add(panImg,BorderLayout.WEST);
      container.add(panButtons);
      
      
      
      this.add(container);
      
      setVisible(true);
    }
   
       
}

 
