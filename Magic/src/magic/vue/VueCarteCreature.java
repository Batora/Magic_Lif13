
package magic.vue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import magic.modele.Creature;

/**
 *
 * @author Marc
 */


public class VueCarteCreature extends JPanel implements Observer{
    
    /**
     * composant qui contient les éléments à afficher d'une carte
     */
    JPanel carte;
    
   
    /**
     * On va récupérer les infos de la carte depuis cet attribut
     */
    Creature carteInfo;
  
    
    /**
     * Tableau de chaines de caractères qui contiendra les informations des cartes
     * @param c la carte Creature posée sur le plateau de jeu
    */    
    public VueCarteCreature(Creature c) {
        this.carteInfo=c;
        initComponent();
    }
    
        
    public void initComponent() {
        
        if(this.carteInfo!=null){
            boolean fatigue = this.carteInfo.isEstFatigue();
            
            /* on récupère les infos d'une carte */
            JLabel nomC = new JLabel("Nom : " + this.carteInfo.getNom() +"\n");
            JLabel coutC = new JLabel("Coût : " + Integer.toString(this.carteInfo.getCout())+"\n");
            JLabel atkC = new JLabel("Atk : " + Integer.toString(this.carteInfo.getAtk())+"\n");
            JLabel defC = new JLabel("Def : " + Integer.toString(this.carteInfo.getDef())+"\n");
            JLabel vide = new JLabel(" ");
            
            GridLayout vueCarteCrea = new GridLayout(3, 3);
            this.setLayout(vueCarteCrea);
            Border blackline = BorderFactory.createLineBorder(Color.black,1);
            for(int i=0;i<3;i++){
                for(int j=0;j<3;j++){
                    if(i==0 && j==0){
                        this.setBorder(blackline);
                        this.add(nomC);
                    }
                    if(i==0 && j==2){
                        this.setBorder(blackline);
                        this.add(coutC);
                    }
                    /* attaque et défense de la carte */
                    if(i==2 && j==0){
                        this.setBorder(blackline);
                        this.add(atkC);
                    }
                    if(i==2 && j==2){
                        this.setBorder(blackline);
                        this.add(defC);
                    }
                    else{
                        this.add(vide);
                    }
                    
                }
            }
            
            if(fatigue){
                // Si la carte est fatiguée elle passe en rouge
                this.setBackground(Color.red);
            }
            
            this.setPreferredSize(new Dimension(150,100));
          
            this.repaint();
            
        }
    }
    
    public Creature getCarte(){
        return this.carteInfo;
    }
    
    @Override
    public void update(Observable o, Object o1) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
    }
}
